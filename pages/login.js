import Image from "next/image";
import { useEffect, useState } from "react";
import styled from "styled-components";
import "bootstrap/dist/css/bootstrap.css";
import Router from "next/router";
import axios from "axios";
import { baseUrl } from "../components/BaseUrl";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Head from "next/head";

const Main = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #edf8fc;
    padding: 24px;
`;

const Content = styled.div`
    .w-50 {
    }
    max-width: 1280px;
    display: flex;
    justify-content: space-between;
    width: 100%;
    @media (max-width: 992px) {
        .w-50 {
            display: none !important;
        }
        justify-content: center;
    }
`;

const LeftContent = styled.div`
    align-items: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

const Box = styled.div`
    width: 400px;
    height: 352px;
    background: #ffffff;
    box-shadow: 0px 2px 8px rgba(50, 50, 50, 0.12);
    border-radius: 16px;
    margin-top: 32px;
    padding: 16px;
    h4 {
        font-weight: 600;
        font-size: 14px;
        margin-top: 20px;
        line-height: 20px;
        color: #323232;
    }
    label {
        display: flex;
        flex-direction: column;
        margin-top: 28px;
        margin-bottom: 8px;
        font-size: 16px;
        line-height: 23px;
    }
    input {
        margin-top: 8px;
        background-color: #edf8fc;
        width: 100%;
        height: 44px;
        border: 1.5px solid #dbdbdb;
        box-sizing: border-box;
        border-radius: 8px;
        padding: 10px;
    }
    @media (max-width: 992px) {
        width: 343px;
    }
`;

const BoxHead = styled.div`
    width: 163px;
    height: 34px;
    background-color: #edf8fc;
    border-radius: 6px;
    padding: 4px;
    button {
        width: 75px;
        height: 100%;
    }
    .login {
        background: #108abb;
        color: #fff;
        border-radius: 10px 4px 4px 10px;
    }
    .register {
        background: #108abb;
        color: #fff;
        border-radius: 4px 10px 10px 4px;
    }
`;

const Submit = styled.button`
    width: 188px;
    height: 38px;
    background: linear-gradient(90deg, #128cbd -1.72%, #3dbdc8 100%);
    border-radius: 32px;
    color: #fff;
    transition: 0.3s all;
    margin-top: 5px;
    :hover {
        opacity: 0.83;
    }
`;

export default function Register() {
    const [activeTab, setActiveTab] = useState("log");
    const [mobile, setMobile] = useState("");
    const [password, setPassword] = useState("");
    const subHandler = async (e) => {
        e.preventDefault();
        let data = {
            mobile: mobile,
            password: password,
            remember: "on",
        };
        let config = {
            method: "POST",
            url: `${baseUrl}token/otp/`,
            data: data,
        };

        await axios(config)
            .then((response) => {
                if (response.status === 200) {
                    localStorage.setItem("id", response.data.id);
                    localStorage.setItem("mobile", mobile);
                    Router.push("/verifycode");
                }
            })
            .catch((error) => {
                toast.error("شماره موبایل یا کلمه عبور اشتباه است", {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            });
    };
    return (
        <Main>
                <ToastContainer
                rtl={true}
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                pauseOnFocusLoss={false}
                draggable
                pauseOnHover={false}
            />
            <Head>
                <title>Login</title>
            </Head>
            <Content>
                <div className="w-50">
                    <Image
                        className="r-img"
                        src="/images/login.png"
                        width={600}
                        height={672}
                        alt="side"
                    />
                </div>
                <LeftContent>
                    <Image
                        src="/images/mob-logo.svg"
                        width={153}
                        height={64}
                        alt="logo"
                    />
                    <Box>
                        <BoxHead>
                            <button
                                onClick={() => {
                                    setActiveTab("log");
                                }}
                                className={activeTab === "log" ? "login" : ""}
                                type="button"
                            >
                                ورود
                            </button>
                            <button
                                onClick={() => {
                                    setActiveTab("reg");
                                    Router.push("/register");
                                }}
                                className={
                                    activeTab === "reg" ? "register" : ""
                                }
                                type="button"
                            >
                                ثبت نام
                            </button>
                        </BoxHead>
                        <h4>ورود به حساب کاربری</h4>

                        <label htmlFor="name">
                            شماره موبایل
                            <input
                                onChange={(e) => {
                                    setMobile(e.target.value);
                                }}
                                type="number"
                                name="phone"
                                id="phone"
                            />
                        </label>
                        <label className="mt-1" htmlFor="password">
                            رمز عبور
                            <input
                                onChange={(e) => {
                                    setPassword(e.target.value);
                                }}
                                type="password"
                                name="password"
                                id="password"
                            />
                        </label>
                        <div className="d-flex justify-content-center mt-3">
                            <Submit onClick={subHandler}>ورود</Submit>
                        </div>
                    </Box>
                </LeftContent>
            </Content>
        </Main>
    );
}
