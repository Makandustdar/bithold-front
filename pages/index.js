import Head from "next/head";
import "bootstrap/dist/css/bootstrap.css";
import Sidebar from "../components/Sidebar";
import styled from "styled-components";
import { useEffect, useState } from "react";
import Router from "next/router";
import Image from "next/image";

const Main = styled.div`
    background-color: #edf8fc;
    width: 100%;
    min-height: 100vh;
    padding: 32px;
    padding-right: 0;
    padding-left: 0;
`;

const HeadMain = styled.div`
    background: #ffffff;
    border-radius: 16px;
    padding: 16px;
    padding-bottom: 60px;
    .mt-60 {
        margin-top: 60px;
        h4 {
            font-size: 28px;
            font-weight: 600;
            span {
                color: #108abb;
            }
        }
        h6 {
            color: #4f4f4f;
            font-weight: 600;
            font-size: 20px;
            line-height: 29px;
            margin-top: 26px;
        }
    }

    @media (max-width: 992px) {
        .mt-2 {
            flex-direction: column-reverse;
        }
        .img {
            object-fit: contain;
            width: 300px !important;
            position: relative !important;
            height: unset !important;
        }
        .img-res {
            width: 100%;
            display: flex;
            justify-content: center;
        }
        .mt-60 {
            margin-top: 32px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;

            h4 {
                font-size: 16px;
                text-align: center;
                font-weight: 600;
                span {
                    color: #108abb;
                }
            }
            h6 {
                font-size: 14px;
                margin-top: 5px;
                text-align: center;
                font-weight: 600;
            }
        }
        .to-center {
            justify-content: center;
            align-items: center;
        }
    }
`;

const Header = styled.header`
    height: 48px;
    width: 100%;
    background: #edf8fc;
    border-radius: 8px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 4px;
    img {
    }

    ul {
        display: flex;
        align-items: center;
        list-style: none;
        margin: 0;
        padding-right: 8px;
        .active-li {
            background: linear-gradient(90deg, #128cbd -1.72%, #3dbdc8 100%);
            border-radius: 4px;
            width: 52px;
            height: 32px;
            display: flex;
            align-items: center;
            justify-content: center;
            color: #fff;
        }
        li {
            cursor: pointer;
            margin-left: 20px;
            color: #323232;
            font-size: 18px;
            line-height: 26px;
        }
    }
    .toggle {
        display: none;
    }
    @media (max-width: 992px) {
        .toggle {
            display: block;
            margin-right: 22px;
        }
        ul {
            display: none;
        }
    }
`;

const Start = styled.button`
    width: 130px;
    height: 42px;
    background: #108abb;
    border-radius: 21px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0 20px;
    color: #fff;
    margin-top: 16px;
    @media (max-width: 992px) {
        width: 114px;
        height: 32px;
        font-size: 14px;
    }
`;

const ChooseShop = styled.div`
    width: 264px;
    height: 55px;
    background: #f9f9f9;
    border-radius: 16px 16px 0px 0px;
    display: flex;
    align-items: center;
    padding-right: 16px;
    padding-left: 8px;
    justify-content: space-between;
    position: relative;
    margin-right: 25px;

    span {
        font-weight: 600;
        font-size: 16px;
        line-height: 23px;
        text-align: right;
        color: #323232;
    }
    .act {
        width: 145px;
        height: 43px;
        background: #dff7ff;
        border-radius: 10px;
        padding: 5px;
        display: flex;
        align-items: center;
        justify-content: space-between;
        span {
            color: #323232;
            font-weight: 600;
            font-size: 17px;
            line-height: 30px;
            text-align: right;
            width: 60px;
            height: 33px;
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;
        }
        .shop-active {
            background: #108abb;
            border-radius: 10px;
            color: #fff;
        }
    }
    .table-wrapper {
        position: absolute;
        max-height: 400px;
        max-width: 1120px;
        top: 55px;
        right: 0;
        overflow: auto;
        display: inline-block;
    }
    table {
        width: 1100px;
        background: #f9f9f9;
        border-radius: 16px 0px 16px 16px;
        white-space: nowrap;
        td,
        th {
            padding-top: 16px;
            padding-right: 16px;
        }
    }
    @media (max-width: 1279px) {
        .table-wrapper {
            width: calc(100vw - 100px) !important;
        }
        table {
            width: calc(100vw - 100px);
        }
    }
    /* @media (max-width: 1050px) {
        .table-wrapper {
            max-width: 700px !important;
        }
    }
    @media (max-width: 992px) {
        position: absolute;
        top: 640px;
        .table-wrapper {
            max-width: 480px !important;
        }
        table {
            width: 820px !important;
        }
    }
     */
    @media (max-width: 786px) {
        margin-top: 50px;
        span {
        font-weight: 600;
        font-size: 14px;
        line-height: 23px;
        text-align: right;
        color: #323232;
    }
    }
`;

const Neg = styled.div`
    width: 69px;
    height: 32px;
    background: rgba(235, 87, 87, 0.2);
    border-radius: 41px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0 10px;
    span {
        color: #eb5757;
    }
`;

const Plus = styled.div`
    width: 69px;
    height: 32px;
    background: rgba(39, 174, 96, 0.2);
    border-radius: 41px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0 10px;
    span {
        color: #27ae60;
    }
`;

const Blog = styled.div`
    margin-top: 380px;
    padding: 32px 50px;
    height: 411px;
    width: 100% !important;
    background: #ffffff;
    -webkit-clip-path: polygon(0 1%, 100% 0, 100% 100%, 0 89%);
    clip-path: polygon(0 1%, 100% 0, 100% 100%, 0 89%);
    .blog-text {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        p {
            font-weight: 600;
            font-size: 22px;
            line-height: 31px;
            text-align: center;
            margin-bottom: 12px;
        }
        span {
            font-weight: normal;
            font-size: 16px;
            line-height: 23px;
            color: #666666;
        }
    }
    @media (max-width: 1200px) {
        height: 100%;
        padding-bottom: 70px;
    }
`;

const BlogCards = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 24px;
    .blog-card {
        position: relative;
        min-width: 200px;
        padding-bottom: 20px;
    }
    .badge {
        position: absolute;
        top: 8px;
        right: 8px;
        width: 58px;
        height: 28px;
        background: #bc4110;
        border-radius: 42px;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .date {
        position: absolute;
        bottom: 38px;
        left: 8px;
        color: #fff;
        font-size: 12px;
    }
    @media (max-width: 1200px) {
        max-width: 1200px;
        overflow: scroll;
        .blog-card {
        }
    }
    /* @media (max-width: 650px) {
        justify-content: center;

    } */
`;

const WhyBithold = styled.div`
    padding: 0px 50px;
    width: 100% !important;
    p {
        font-weight: 600;
        font-size: 22px;
        line-height: 31px;
        text-align: center;
        margin-bottom: 12px;
    }
    span {
        font-weight: normal;
        font-size: 16px;
        line-height: 23px;
        color: #666666;
    }
    .card {
        margin: 0 10px;
        min-width: 352px;
        height: 150px;
        background: #ffffff;
        box-shadow: 0px 2px 18px rgba(61, 189, 200, 0.1);
        border-radius: 8px;
        padding: 33px 20px;
        display: flex !important;
        text-align: right;
        flex-direction: row;
        overflow: visible;
        justify-content: space-between;
        align-items: center;
        margin-top: 24px;
        h6 {
            font-weight: 600;
            font-size: 16px;
            line-height: 23px;
            color: #323232;
            margin-right: 10px;
        }
        p {
            font-size: 14px;
            margin-right: 10px;
            line-height: 20px;
            text-align: right;
            color: #474747;
        }
    }
    @media (max-width: 1200px) {
        .cardss {
            margin-top: 40px;
            max-width: 1200px;
            overflow: scroll;
            padding-bottom: 20px;
        }
        .card {
        }
    }
    @media (max-width: 810px) {
        .card {
            h6 {
                font-size: 14px;
            }
            p {
                font-size: 12px;
            }
        }
    }
`;

const Application = styled.div`
    margin-top: 24px;
    padding: 43px 50px;
    height: 411px;
    width: 100% !important;
    background: #ffffff;
    -webkit-clip-path: polygon(0 0, 100% 12%, 100% 100%, 0% 100%);
    clip-path: polygon(0 0, 100% 12%, 100% 100%, 0% 100%);
    display: flex;
    align-items: center;
    justify-content: space-between;
    @media (max-width: 550px) {
        flex-direction: column-reverse;
        height: 100%;
    }
`;

const App = styled.div`
    text-align: right;
    h4 {
        font-weight: 600;
        font-size: 22px;
        line-height: 31px;
        color: #323232;
        margin-top: 30px;
    }
    p {
        margin-top: 20px;
        font-size: 17px;
        line-height: 24px;
        color: #323232;
    }
    img {
        cursor: pointer;
    }
    @media (max-width: 550px) {
        h4 {
            font-size: 18px;
            text-align: center;
        }
        p {
            font-size: 14px;
            text-align: center;
            margin-top: 0;
        }
    }
`;

const Footer = styled.footer`
    max-width: 1120px;
    margin-top: 60px;
    margin-right: auto;
    margin-left: auto;
    height: 200px;
    background: #ffffff;
    border-radius: 8px;
    position: relative;
    display: flex;
    padding: 32px 24px;
    h5 {
        font-weight: 600;
        font-size: 16px;
        line-height: 20px;
        color: #323232;
    }
    p {
        font-weight: normal;
        font-size: 14px;
        line-height: 20px;
        text-align: right;
        color: #323232;
        max-width: 300px;
    }

    .footer-col-2 {
        margin-right: 60px;
    }
    .footer-col-4 {
        margin-right: 60px;
    }
    input {
        width: 256px;
        height: 38px;
        margin-top: 8px;
        background: #edf8fc;
        border-radius: 8px;
        padding: 16px;
        direction: ltr;
    }
    button {
        width: 102px;
        height: 24px;
        background: #108abb;
        border-radius: 32px;
        color: #fff;
        margin-top: 16px;
        transition: 0.3s all;
        :hover {
            background: #108bbbc5;
        }
    }
    @media (max-width: 1150px) {
        flex-direction: column;
        height: 100%;
        align-items: center;
        .res-mt {
            margin-top: 50px;
        }
    }
    @media (max-width: 650px) {
        .d-flex {
            flex-direction: column;
        }
        .footer-col-2,
        .footer-col-4 {
            margin-top: 30px;
            margin-right: 0;
        }
    }
`;

const CopyRight = styled.div`
    font-weight: 600;
    font-size: 12px;
    line-height: 17px;
    text-align: center;
    margin-top: 16px;
    color: #777777;
`;

const ToogleMenu = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    background-color: #fff;
    transition: 0.5s all;
    height: 100vh;
    width: 100vw;
    z-index: 1;
    overflow: hidden;
    ul {
        list-style: none;
        margin-top: 50px;
        li {
            margin-bottom: 20px;
        }
    }
`;

export default function Home() {
    const [isLogin, setIsLogin] = useState(false);
    const [shop, setShop] = useState("TMN");
    const [showMenu, setShowMenu] = useState(false);
    const isLoginHandler = (e) => {
        if (
            localStorage.getItem("token") == null ||
            typeof window == "undefined"
        ) {
            Router.push("/login");
        } else {
            Router.push("/dashboard");
        }
    };
    return (
        <Main className="max-w-1992">
            <Head>
                <title>Bit Hold</title>
            </Head>
            <ToogleMenu className={showMenu ? "w-100-vw" : "w-0"}>
                <div className="d-flex justify-content-between">
                    <div></div>
                    <svg
                        className="close"
                        onClick={() => {
                            setShowMenu(false);
                        }}
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        viewBox="0 0 14 14"
                    >
                        <path
                            id="ic_close_24px"
                            d="M19,6.41,17.59,5,12,10.59,6.41,5,5,6.41,10.59,12,5,17.59,6.41,19,12,13.41,17.59,19,19,17.59,13.41,12Z"
                            transform="translate(-5 -5)"
                        ></path>
                    </svg>
                </div>
                <ul>
                    <li className="active-li">خانه</li>
                    <li onClick={isLoginHandler}>بازارها</li>
                    <li onClick={isLoginHandler}>مبدل ارزها</li>
                    <li>تماس با ما</li>
                </ul>
            </ToogleMenu>
            <div className="px-4">
                <HeadMain>
                    <Header>
                        <div
                            onClick={() => {
                                setShowMenu(true);
                            }}
                            className="toggle"
                        >
                            <svg
                                width="20"
                                height="14"
                                viewBox="0 0 20 14"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    fillRule="evenodd"
                                    clipRule="evenodd"
                                    d="M0.330322 7C0.330322 6.44772 0.778037 6 1.33032 6H18.6704C19.2227 6 19.6704 6.44772 19.6704 7C19.6704 7.55229 19.2227 8 18.6704 8H1.33032C0.778037 8 0.330322 7.55229 0.330322 7Z"
                                    fill="#323232"
                                />
                                <path
                                    fillRule="evenodd"
                                    clipRule="evenodd"
                                    d="M0.330322 12.3356C0.330322 11.7833 0.778037 11.3356 1.33032 11.3356H18.6704C19.2227 11.3356 19.6704 11.7833 19.6704 12.3356C19.6704 12.8879 19.2227 13.3356 18.6704 13.3356H1.33032C0.778037 13.3356 0.330322 12.8879 0.330322 12.3356Z"
                                    fill="#323232"
                                />
                                <path
                                    fillRule="evenodd"
                                    clipRule="evenodd"
                                    d="M0.329834 1.66443C0.329834 1.11214 0.777549 0.664429 1.32983 0.664429H18.6699C19.2222 0.664429 19.6699 1.11214 19.6699 1.66443C19.6699 2.21671 19.2222 2.66443 18.6699 2.66443H1.32983C0.777549 2.66443 0.329834 2.21671 0.329834 1.66443Z"
                                    fill="#323232"
                                />
                            </svg>
                        </div>
                        <ul>
                            <li className="active-li">خانه</li>
                            <li onClick={isLoginHandler}>بازارها</li>
                            <li onClick={isLoginHandler}>مبدل ارزها</li>
                            <li>تماس با ما</li>
                        </ul>
                        <div className="ms-2 d-flex align-items-center">
                            <Image
                                alt="logo"
                                src="/images/header-logo.png"
                                width={97}
                                height={40}
                            />
                        </div>
                    </Header>
                    <div className="d-flex to-center mt-2 justify-content-between">
                        <div className="mt-60">
                            <h4>
                                خرید و فروش امن <span>بیت کوین </span> و
                                <span> ارزهای دیجیتال</span>
                            </h4>
                            <h6>
                                به بزرگترین بازار ارز دیجیتال ایران بپیوندید
                            </h6>
                            <Start onClick={isLoginHandler}>
                                شروع کنید
                                <svg
                                    width="6"
                                    height="10"
                                    viewBox="0 0 6 10"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        d="M5 1L1 5L5 9"
                                        stroke="white"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                </svg>
                            </Start>
                        </div>
                        <div className="img-res">
                            <Image
                                src="/images/landing-head-left.png"
                                width={380}
                                height={350}
                                alt="coins"
                            />
                        </div>
                    </div>
                    <ChooseShop>
                        <span>انتخاب بازار :</span>
                        <div className="act">
                            <span
                                className={shop === "TMN" ? "shop-active" : ""}
                                onClick={() => {
                                    setShop("TMN");
                                }}
                            >
                                TMN
                            </span>
                            <span
                                className={shop === "USDT" ? "shop-active" : ""}
                                onClick={() => {
                                    setShop("USDT");
                                }}
                            >
                                USDT
                            </span>
                        </div>
                        <div className="table-wrapper">
                            <table>
                                <thead>
                                    <tr>
                                        <th className="d-flex align-items-center">
                                            <svg
                                                className="ms-1"
                                                width="10"
                                                height="6"
                                                viewBox="0 0 10 6"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M1 1L5 5L9 1"
                                                    stroke="#323232"
                                                    strokeWidth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                            </svg>
                                            #
                                        </th>
                                        <th className="align-middle">
                                            نام ارز
                                        </th>
                                        <th className="align-middle">
                                            آخرین قیمت
                                        </th>
                                        <th className="align-middle">
                                            تغییرات 24 ساعته
                                        </th>
                                        <th className="align-middle">
                                            تغییرات 7 روز
                                        </th>
                                        <th className="align-middle">
                                            نمودار هفتگی
                                        </th>
                                        <th className="align-middle"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="align-middle">1</td>
                                        <td className="align-middle">
                                            <svg
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    opacity="0.2"
                                                    d="M31.52 19.8708C29.383 28.4423 20.701 33.6583 12.1285 31.5213C3.55999 29.3843 -1.65651 20.7023 0.48099 12.1313C2.61699 3.5588 11.2985 -1.6582 19.8685 0.478798C28.4405 2.6158 33.6565 11.2988 31.5195 19.8708H31.52Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    fillRule="evenodd"
                                                    clipRule="evenodd"
                                                    d="M20.8187 14.1411C21.0344 12.6969 19.9347 11.9205 18.4311 11.4025L18.9189 9.44604L17.7276 9.14925L17.2527 11.0542C16.94 10.9761 16.6185 10.9025 16.2989 10.8296L16.7772 8.91215L15.587 8.61536L15.0989 10.5711C14.8397 10.5121 14.5853 10.4538 14.3384 10.3924L14.3398 10.3863L12.6974 9.9762L12.3806 11.2482C12.3806 11.2482 13.2642 11.4507 13.2455 11.4632C13.7279 11.5836 13.8154 11.9028 13.8004 12.1559L13.2448 14.3847C13.2781 14.3932 13.3212 14.4054 13.3686 14.4244L13.2431 14.3932L12.464 17.5154C12.405 17.662 12.2554 17.8818 11.9183 17.7983C11.9301 17.8156 11.0526 17.5823 11.0526 17.5823L10.4614 18.9458L12.0115 19.3322C12.1865 19.376 12.3594 19.4211 12.5304 19.4657L12.5304 19.4657L12.5308 19.4658C12.6414 19.4947 12.7511 19.5233 12.8602 19.5513L12.3673 21.5305L13.5569 21.8273L14.0453 19.8694C14.3699 19.9576 14.6854 20.039 14.9941 20.1157L14.5077 22.0643L15.6986 22.3611L16.1914 20.386C18.2222 20.7703 19.7495 20.6153 20.3916 18.7789C20.9096 17.3 20.3662 16.447 19.2977 15.8904C20.0758 15.7103 20.662 15.1984 20.8183 14.1411H20.8187ZM18.0973 17.9567C17.7592 19.3141 15.6342 18.7524 14.6628 18.4957L14.6627 18.4957C14.5757 18.4727 14.498 18.4522 14.4317 18.4357L15.0856 15.814C15.1668 15.8343 15.2659 15.8565 15.3781 15.8817L15.3782 15.8817C16.3832 16.1073 18.4427 16.5694 18.0977 17.9567H18.0973ZM15.5802 14.6652C16.3907 14.8814 18.1576 15.3528 18.4654 14.1198C18.78 12.8583 17.062 12.4781 16.2229 12.2925C16.1286 12.2716 16.0454 12.2532 15.9774 12.2362L15.3845 14.614C15.4406 14.628 15.5064 14.6455 15.5802 14.6652Z"
                                                    fill="#F7931A"
                                                />
                                            </svg>
                                            <span className="me-2">
                                                بیت کوین <span>(BTC)</span>
                                            </span>
                                        </td>
                                        <td className="align-middle">
                                            <span>1,102,000</span>
                                        </td>
                                        <td>
                                            <Neg>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M1 1L5 5L9 1"
                                                        stroke="#EB5757"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Neg>
                                        </td>
                                        <td>
                                            <Plus>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M9 5L5 1L1 5"
                                                        stroke="#27AE60"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Plus>
                                        </td>
                                        <td>
                                            <Image
                                                src="/images/landing-chart.png"
                                                width={111}
                                                height={33}
                                                alt="chart"
                                            />
                                        </td>
                                        <td>
                                            <svg
                                                width="26"
                                                height="26"
                                                viewBox="0 0 26 26"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M13 6.49733C13.2762 6.49733 13.5002 6.72128 13.5002 6.99753C13.5002 7.27379 13.2762 7.49774 13 7.49774C12.7237 7.49774 12.4998 7.27379 12.4998 6.99753C12.4998 6.72128 12.7237 6.49733 13 6.49733"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 12.4998C13.2762 12.4998 13.5002 12.7238 13.5002 13C13.5002 13.2763 13.2762 13.5002 13 13.5002C12.7237 13.5002 12.4998 13.2763 12.4998 13C12.4998 12.7238 12.7237 12.4998 13 12.4998"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 18.5023C13.2762 18.5023 13.5002 18.7262 13.5002 19.0025C13.5002 19.2787 13.2762 19.5027 13 19.5027C12.7237 19.5027 12.4998 19.2787 12.4998 19.0025C12.4998 18.7262 12.7237 18.5023 13 18.5023"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                            </svg>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="align-middle">1</td>
                                        <td className="align-middle">
                                            <svg
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    opacity="0.2"
                                                    d="M31.52 19.8708C29.383 28.4423 20.701 33.6583 12.1285 31.5213C3.55999 29.3843 -1.65651 20.7023 0.48099 12.1313C2.61699 3.5588 11.2985 -1.6582 19.8685 0.478798C28.4405 2.6158 33.6565 11.2988 31.5195 19.8708H31.52Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    fillRule="evenodd"
                                                    clipRule="evenodd"
                                                    d="M20.8187 14.1411C21.0344 12.6969 19.9347 11.9205 18.4311 11.4025L18.9189 9.44604L17.7276 9.14925L17.2527 11.0542C16.94 10.9761 16.6185 10.9025 16.2989 10.8296L16.7772 8.91215L15.587 8.61536L15.0989 10.5711C14.8397 10.5121 14.5853 10.4538 14.3384 10.3924L14.3398 10.3863L12.6974 9.9762L12.3806 11.2482C12.3806 11.2482 13.2642 11.4507 13.2455 11.4632C13.7279 11.5836 13.8154 11.9028 13.8004 12.1559L13.2448 14.3847C13.2781 14.3932 13.3212 14.4054 13.3686 14.4244L13.2431 14.3932L12.464 17.5154C12.405 17.662 12.2554 17.8818 11.9183 17.7983C11.9301 17.8156 11.0526 17.5823 11.0526 17.5823L10.4614 18.9458L12.0115 19.3322C12.1865 19.376 12.3594 19.4211 12.5304 19.4657L12.5304 19.4657L12.5308 19.4658C12.6414 19.4947 12.7511 19.5233 12.8602 19.5513L12.3673 21.5305L13.5569 21.8273L14.0453 19.8694C14.3699 19.9576 14.6854 20.039 14.9941 20.1157L14.5077 22.0643L15.6986 22.3611L16.1914 20.386C18.2222 20.7703 19.7495 20.6153 20.3916 18.7789C20.9096 17.3 20.3662 16.447 19.2977 15.8904C20.0758 15.7103 20.662 15.1984 20.8183 14.1411H20.8187ZM18.0973 17.9567C17.7592 19.3141 15.6342 18.7524 14.6628 18.4957L14.6627 18.4957C14.5757 18.4727 14.498 18.4522 14.4317 18.4357L15.0856 15.814C15.1668 15.8343 15.2659 15.8565 15.3781 15.8817L15.3782 15.8817C16.3832 16.1073 18.4427 16.5694 18.0977 17.9567H18.0973ZM15.5802 14.6652C16.3907 14.8814 18.1576 15.3528 18.4654 14.1198C18.78 12.8583 17.062 12.4781 16.2229 12.2925C16.1286 12.2716 16.0454 12.2532 15.9774 12.2362L15.3845 14.614C15.4406 14.628 15.5064 14.6455 15.5802 14.6652Z"
                                                    fill="#F7931A"
                                                />
                                            </svg>
                                            <span className="me-2">
                                                بیت کوین <span>(BTC)</span>
                                            </span>
                                        </td>
                                        <td className="align-middle">
                                            <span>1,102,000</span>
                                        </td>
                                        <td>
                                            <Neg>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M1 1L5 5L9 1"
                                                        stroke="#EB5757"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Neg>
                                        </td>
                                        <td>
                                            <Plus>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M9 5L5 1L1 5"
                                                        stroke="#27AE60"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Plus>
                                        </td>
                                        <td>
                                            <Image
                                                src="/images/landing-chart.png"
                                                width={111}
                                                height={33}
                                                alt="chart"
                                            />
                                        </td>
                                        <td>
                                            <svg
                                                width="26"
                                                height="26"
                                                viewBox="0 0 26 26"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M13 6.49733C13.2762 6.49733 13.5002 6.72128 13.5002 6.99753C13.5002 7.27379 13.2762 7.49774 13 7.49774C12.7237 7.49774 12.4998 7.27379 12.4998 6.99753C12.4998 6.72128 12.7237 6.49733 13 6.49733"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 12.4998C13.2762 12.4998 13.5002 12.7238 13.5002 13C13.5002 13.2763 13.2762 13.5002 13 13.5002C12.7237 13.5002 12.4998 13.2763 12.4998 13C12.4998 12.7238 12.7237 12.4998 13 12.4998"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 18.5023C13.2762 18.5023 13.5002 18.7262 13.5002 19.0025C13.5002 19.2787 13.2762 19.5027 13 19.5027C12.7237 19.5027 12.4998 19.2787 12.4998 19.0025C12.4998 18.7262 12.7237 18.5023 13 18.5023"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                            </svg>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="align-middle">1</td>
                                        <td className="align-middle">
                                            <svg
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    opacity="0.2"
                                                    d="M31.52 19.8708C29.383 28.4423 20.701 33.6583 12.1285 31.5213C3.55999 29.3843 -1.65651 20.7023 0.48099 12.1313C2.61699 3.5588 11.2985 -1.6582 19.8685 0.478798C28.4405 2.6158 33.6565 11.2988 31.5195 19.8708H31.52Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    fillRule="evenodd"
                                                    clipRule="evenodd"
                                                    d="M20.8187 14.1411C21.0344 12.6969 19.9347 11.9205 18.4311 11.4025L18.9189 9.44604L17.7276 9.14925L17.2527 11.0542C16.94 10.9761 16.6185 10.9025 16.2989 10.8296L16.7772 8.91215L15.587 8.61536L15.0989 10.5711C14.8397 10.5121 14.5853 10.4538 14.3384 10.3924L14.3398 10.3863L12.6974 9.9762L12.3806 11.2482C12.3806 11.2482 13.2642 11.4507 13.2455 11.4632C13.7279 11.5836 13.8154 11.9028 13.8004 12.1559L13.2448 14.3847C13.2781 14.3932 13.3212 14.4054 13.3686 14.4244L13.2431 14.3932L12.464 17.5154C12.405 17.662 12.2554 17.8818 11.9183 17.7983C11.9301 17.8156 11.0526 17.5823 11.0526 17.5823L10.4614 18.9458L12.0115 19.3322C12.1865 19.376 12.3594 19.4211 12.5304 19.4657L12.5304 19.4657L12.5308 19.4658C12.6414 19.4947 12.7511 19.5233 12.8602 19.5513L12.3673 21.5305L13.5569 21.8273L14.0453 19.8694C14.3699 19.9576 14.6854 20.039 14.9941 20.1157L14.5077 22.0643L15.6986 22.3611L16.1914 20.386C18.2222 20.7703 19.7495 20.6153 20.3916 18.7789C20.9096 17.3 20.3662 16.447 19.2977 15.8904C20.0758 15.7103 20.662 15.1984 20.8183 14.1411H20.8187ZM18.0973 17.9567C17.7592 19.3141 15.6342 18.7524 14.6628 18.4957L14.6627 18.4957C14.5757 18.4727 14.498 18.4522 14.4317 18.4357L15.0856 15.814C15.1668 15.8343 15.2659 15.8565 15.3781 15.8817L15.3782 15.8817C16.3832 16.1073 18.4427 16.5694 18.0977 17.9567H18.0973ZM15.5802 14.6652C16.3907 14.8814 18.1576 15.3528 18.4654 14.1198C18.78 12.8583 17.062 12.4781 16.2229 12.2925C16.1286 12.2716 16.0454 12.2532 15.9774 12.2362L15.3845 14.614C15.4406 14.628 15.5064 14.6455 15.5802 14.6652Z"
                                                    fill="#F7931A"
                                                />
                                            </svg>
                                            <span className="me-2">
                                                بیت کوین <span>(BTC)</span>
                                            </span>
                                        </td>
                                        <td className="align-middle">
                                            <span>1,102,000</span>
                                        </td>
                                        <td>
                                            <Neg>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M1 1L5 5L9 1"
                                                        stroke="#EB5757"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Neg>
                                        </td>
                                        <td>
                                            <Plus>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M9 5L5 1L1 5"
                                                        stroke="#27AE60"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Plus>
                                        </td>
                                        <td>
                                            <Image
                                                src="/images/landing-chart.png"
                                                width={111}
                                                height={33}
                                                alt="chart"
                                            />
                                        </td>
                                        <td>
                                            <svg
                                                width="26"
                                                height="26"
                                                viewBox="0 0 26 26"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M13 6.49733C13.2762 6.49733 13.5002 6.72128 13.5002 6.99753C13.5002 7.27379 13.2762 7.49774 13 7.49774C12.7237 7.49774 12.4998 7.27379 12.4998 6.99753C12.4998 6.72128 12.7237 6.49733 13 6.49733"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 12.4998C13.2762 12.4998 13.5002 12.7238 13.5002 13C13.5002 13.2763 13.2762 13.5002 13 13.5002C12.7237 13.5002 12.4998 13.2763 12.4998 13C12.4998 12.7238 12.7237 12.4998 13 12.4998"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 18.5023C13.2762 18.5023 13.5002 18.7262 13.5002 19.0025C13.5002 19.2787 13.2762 19.5027 13 19.5027C12.7237 19.5027 12.4998 19.2787 12.4998 19.0025C12.4998 18.7262 12.7237 18.5023 13 18.5023"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                            </svg>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="align-middle">1</td>
                                        <td className="align-middle">
                                            <svg
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    opacity="0.2"
                                                    d="M31.52 19.8708C29.383 28.4423 20.701 33.6583 12.1285 31.5213C3.55999 29.3843 -1.65651 20.7023 0.48099 12.1313C2.61699 3.5588 11.2985 -1.6582 19.8685 0.478798C28.4405 2.6158 33.6565 11.2988 31.5195 19.8708H31.52Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    fillRule="evenodd"
                                                    clipRule="evenodd"
                                                    d="M20.8187 14.1411C21.0344 12.6969 19.9347 11.9205 18.4311 11.4025L18.9189 9.44604L17.7276 9.14925L17.2527 11.0542C16.94 10.9761 16.6185 10.9025 16.2989 10.8296L16.7772 8.91215L15.587 8.61536L15.0989 10.5711C14.8397 10.5121 14.5853 10.4538 14.3384 10.3924L14.3398 10.3863L12.6974 9.9762L12.3806 11.2482C12.3806 11.2482 13.2642 11.4507 13.2455 11.4632C13.7279 11.5836 13.8154 11.9028 13.8004 12.1559L13.2448 14.3847C13.2781 14.3932 13.3212 14.4054 13.3686 14.4244L13.2431 14.3932L12.464 17.5154C12.405 17.662 12.2554 17.8818 11.9183 17.7983C11.9301 17.8156 11.0526 17.5823 11.0526 17.5823L10.4614 18.9458L12.0115 19.3322C12.1865 19.376 12.3594 19.4211 12.5304 19.4657L12.5304 19.4657L12.5308 19.4658C12.6414 19.4947 12.7511 19.5233 12.8602 19.5513L12.3673 21.5305L13.5569 21.8273L14.0453 19.8694C14.3699 19.9576 14.6854 20.039 14.9941 20.1157L14.5077 22.0643L15.6986 22.3611L16.1914 20.386C18.2222 20.7703 19.7495 20.6153 20.3916 18.7789C20.9096 17.3 20.3662 16.447 19.2977 15.8904C20.0758 15.7103 20.662 15.1984 20.8183 14.1411H20.8187ZM18.0973 17.9567C17.7592 19.3141 15.6342 18.7524 14.6628 18.4957L14.6627 18.4957C14.5757 18.4727 14.498 18.4522 14.4317 18.4357L15.0856 15.814C15.1668 15.8343 15.2659 15.8565 15.3781 15.8817L15.3782 15.8817C16.3832 16.1073 18.4427 16.5694 18.0977 17.9567H18.0973ZM15.5802 14.6652C16.3907 14.8814 18.1576 15.3528 18.4654 14.1198C18.78 12.8583 17.062 12.4781 16.2229 12.2925C16.1286 12.2716 16.0454 12.2532 15.9774 12.2362L15.3845 14.614C15.4406 14.628 15.5064 14.6455 15.5802 14.6652Z"
                                                    fill="#F7931A"
                                                />
                                            </svg>
                                            <span className="me-2">
                                                بیت کوین <span>(BTC)</span>
                                            </span>
                                        </td>
                                        <td className="align-middle">
                                            <span>1,102,000</span>
                                        </td>
                                        <td>
                                            <Neg>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M1 1L5 5L9 1"
                                                        stroke="#EB5757"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Neg>
                                        </td>
                                        <td>
                                            <Plus>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M9 5L5 1L1 5"
                                                        stroke="#27AE60"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Plus>
                                        </td>
                                        <td>
                                            <Image
                                                src="/images/landing-chart.png"
                                                width={111}
                                                height={33}
                                                alt="chart"
                                            />
                                        </td>
                                        <td>
                                            <svg
                                                width="26"
                                                height="26"
                                                viewBox="0 0 26 26"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M13 6.49733C13.2762 6.49733 13.5002 6.72128 13.5002 6.99753C13.5002 7.27379 13.2762 7.49774 13 7.49774C12.7237 7.49774 12.4998 7.27379 12.4998 6.99753C12.4998 6.72128 12.7237 6.49733 13 6.49733"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 12.4998C13.2762 12.4998 13.5002 12.7238 13.5002 13C13.5002 13.2763 13.2762 13.5002 13 13.5002C12.7237 13.5002 12.4998 13.2763 12.4998 13C12.4998 12.7238 12.7237 12.4998 13 12.4998"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 18.5023C13.2762 18.5023 13.5002 18.7262 13.5002 19.0025C13.5002 19.2787 13.2762 19.5027 13 19.5027C12.7237 19.5027 12.4998 19.2787 12.4998 19.0025C12.4998 18.7262 12.7237 18.5023 13 18.5023"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                            </svg>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="align-middle">1</td>
                                        <td className="align-middle">
                                            <svg
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    opacity="0.2"
                                                    d="M31.52 19.8708C29.383 28.4423 20.701 33.6583 12.1285 31.5213C3.55999 29.3843 -1.65651 20.7023 0.48099 12.1313C2.61699 3.5588 11.2985 -1.6582 19.8685 0.478798C28.4405 2.6158 33.6565 11.2988 31.5195 19.8708H31.52Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    fillRule="evenodd"
                                                    clipRule="evenodd"
                                                    d="M20.8187 14.1411C21.0344 12.6969 19.9347 11.9205 18.4311 11.4025L18.9189 9.44604L17.7276 9.14925L17.2527 11.0542C16.94 10.9761 16.6185 10.9025 16.2989 10.8296L16.7772 8.91215L15.587 8.61536L15.0989 10.5711C14.8397 10.5121 14.5853 10.4538 14.3384 10.3924L14.3398 10.3863L12.6974 9.9762L12.3806 11.2482C12.3806 11.2482 13.2642 11.4507 13.2455 11.4632C13.7279 11.5836 13.8154 11.9028 13.8004 12.1559L13.2448 14.3847C13.2781 14.3932 13.3212 14.4054 13.3686 14.4244L13.2431 14.3932L12.464 17.5154C12.405 17.662 12.2554 17.8818 11.9183 17.7983C11.9301 17.8156 11.0526 17.5823 11.0526 17.5823L10.4614 18.9458L12.0115 19.3322C12.1865 19.376 12.3594 19.4211 12.5304 19.4657L12.5304 19.4657L12.5308 19.4658C12.6414 19.4947 12.7511 19.5233 12.8602 19.5513L12.3673 21.5305L13.5569 21.8273L14.0453 19.8694C14.3699 19.9576 14.6854 20.039 14.9941 20.1157L14.5077 22.0643L15.6986 22.3611L16.1914 20.386C18.2222 20.7703 19.7495 20.6153 20.3916 18.7789C20.9096 17.3 20.3662 16.447 19.2977 15.8904C20.0758 15.7103 20.662 15.1984 20.8183 14.1411H20.8187ZM18.0973 17.9567C17.7592 19.3141 15.6342 18.7524 14.6628 18.4957L14.6627 18.4957C14.5757 18.4727 14.498 18.4522 14.4317 18.4357L15.0856 15.814C15.1668 15.8343 15.2659 15.8565 15.3781 15.8817L15.3782 15.8817C16.3832 16.1073 18.4427 16.5694 18.0977 17.9567H18.0973ZM15.5802 14.6652C16.3907 14.8814 18.1576 15.3528 18.4654 14.1198C18.78 12.8583 17.062 12.4781 16.2229 12.2925C16.1286 12.2716 16.0454 12.2532 15.9774 12.2362L15.3845 14.614C15.4406 14.628 15.5064 14.6455 15.5802 14.6652Z"
                                                    fill="#F7931A"
                                                />
                                            </svg>
                                            <span className="me-2">
                                                بیت کوین <span>(BTC)</span>
                                            </span>
                                        </td>
                                        <td className="align-middle">
                                            <span>1,102,000</span>
                                        </td>
                                        <td>
                                            <Neg>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M1 1L5 5L9 1"
                                                        stroke="#EB5757"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Neg>
                                        </td>
                                        <td>
                                            <Plus>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M9 5L5 1L1 5"
                                                        stroke="#27AE60"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Plus>
                                        </td>
                                        <td>
                                            <Image
                                                src="/images/landing-chart.png"
                                                width={111}
                                                height={33}
                                                alt="chart"
                                            />
                                        </td>
                                        <td>
                                            <svg
                                                width="26"
                                                height="26"
                                                viewBox="0 0 26 26"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M13 6.49733C13.2762 6.49733 13.5002 6.72128 13.5002 6.99753C13.5002 7.27379 13.2762 7.49774 13 7.49774C12.7237 7.49774 12.4998 7.27379 12.4998 6.99753C12.4998 6.72128 12.7237 6.49733 13 6.49733"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 12.4998C13.2762 12.4998 13.5002 12.7238 13.5002 13C13.5002 13.2763 13.2762 13.5002 13 13.5002C12.7237 13.5002 12.4998 13.2763 12.4998 13C12.4998 12.7238 12.7237 12.4998 13 12.4998"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 18.5023C13.2762 18.5023 13.5002 18.7262 13.5002 19.0025C13.5002 19.2787 13.2762 19.5027 13 19.5027C12.7237 19.5027 12.4998 19.2787 12.4998 19.0025C12.4998 18.7262 12.7237 18.5023 13 18.5023"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                            </svg>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="align-middle">1</td>
                                        <td className="align-middle">
                                            <svg
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    opacity="0.2"
                                                    d="M31.52 19.8708C29.383 28.4423 20.701 33.6583 12.1285 31.5213C3.55999 29.3843 -1.65651 20.7023 0.48099 12.1313C2.61699 3.5588 11.2985 -1.6582 19.8685 0.478798C28.4405 2.6158 33.6565 11.2988 31.5195 19.8708H31.52Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    fillRule="evenodd"
                                                    clipRule="evenodd"
                                                    d="M20.8187 14.1411C21.0344 12.6969 19.9347 11.9205 18.4311 11.4025L18.9189 9.44604L17.7276 9.14925L17.2527 11.0542C16.94 10.9761 16.6185 10.9025 16.2989 10.8296L16.7772 8.91215L15.587 8.61536L15.0989 10.5711C14.8397 10.5121 14.5853 10.4538 14.3384 10.3924L14.3398 10.3863L12.6974 9.9762L12.3806 11.2482C12.3806 11.2482 13.2642 11.4507 13.2455 11.4632C13.7279 11.5836 13.8154 11.9028 13.8004 12.1559L13.2448 14.3847C13.2781 14.3932 13.3212 14.4054 13.3686 14.4244L13.2431 14.3932L12.464 17.5154C12.405 17.662 12.2554 17.8818 11.9183 17.7983C11.9301 17.8156 11.0526 17.5823 11.0526 17.5823L10.4614 18.9458L12.0115 19.3322C12.1865 19.376 12.3594 19.4211 12.5304 19.4657L12.5304 19.4657L12.5308 19.4658C12.6414 19.4947 12.7511 19.5233 12.8602 19.5513L12.3673 21.5305L13.5569 21.8273L14.0453 19.8694C14.3699 19.9576 14.6854 20.039 14.9941 20.1157L14.5077 22.0643L15.6986 22.3611L16.1914 20.386C18.2222 20.7703 19.7495 20.6153 20.3916 18.7789C20.9096 17.3 20.3662 16.447 19.2977 15.8904C20.0758 15.7103 20.662 15.1984 20.8183 14.1411H20.8187ZM18.0973 17.9567C17.7592 19.3141 15.6342 18.7524 14.6628 18.4957L14.6627 18.4957C14.5757 18.4727 14.498 18.4522 14.4317 18.4357L15.0856 15.814C15.1668 15.8343 15.2659 15.8565 15.3781 15.8817L15.3782 15.8817C16.3832 16.1073 18.4427 16.5694 18.0977 17.9567H18.0973ZM15.5802 14.6652C16.3907 14.8814 18.1576 15.3528 18.4654 14.1198C18.78 12.8583 17.062 12.4781 16.2229 12.2925C16.1286 12.2716 16.0454 12.2532 15.9774 12.2362L15.3845 14.614C15.4406 14.628 15.5064 14.6455 15.5802 14.6652Z"
                                                    fill="#F7931A"
                                                />
                                            </svg>
                                            <span className="me-2">
                                                بیت کوین <span>(BTC)</span>
                                            </span>
                                        </td>
                                        <td className="align-middle">
                                            <span>1,102,000</span>
                                        </td>
                                        <td>
                                            <Neg>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M1 1L5 5L9 1"
                                                        stroke="#EB5757"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Neg>
                                        </td>
                                        <td>
                                            <Plus>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M9 5L5 1L1 5"
                                                        stroke="#27AE60"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Plus>
                                        </td>
                                        <td>
                                            <Image
                                                src="/images/landing-chart.png"
                                                width={111}
                                                height={33}
                                                alt="chart"
                                            />
                                        </td>
                                        <td>
                                            <svg
                                                width="26"
                                                height="26"
                                                viewBox="0 0 26 26"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M13 6.49733C13.2762 6.49733 13.5002 6.72128 13.5002 6.99753C13.5002 7.27379 13.2762 7.49774 13 7.49774C12.7237 7.49774 12.4998 7.27379 12.4998 6.99753C12.4998 6.72128 12.7237 6.49733 13 6.49733"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 12.4998C13.2762 12.4998 13.5002 12.7238 13.5002 13C13.5002 13.2763 13.2762 13.5002 13 13.5002C12.7237 13.5002 12.4998 13.2763 12.4998 13C12.4998 12.7238 12.7237 12.4998 13 12.4998"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 18.5023C13.2762 18.5023 13.5002 18.7262 13.5002 19.0025C13.5002 19.2787 13.2762 19.5027 13 19.5027C12.7237 19.5027 12.4998 19.2787 12.4998 19.0025C12.4998 18.7262 12.7237 18.5023 13 18.5023"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                            </svg>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="align-middle">1</td>
                                        <td className="align-middle">
                                            <svg
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    opacity="0.2"
                                                    d="M31.52 19.8708C29.383 28.4423 20.701 33.6583 12.1285 31.5213C3.55999 29.3843 -1.65651 20.7023 0.48099 12.1313C2.61699 3.5588 11.2985 -1.6582 19.8685 0.478798C28.4405 2.6158 33.6565 11.2988 31.5195 19.8708H31.52Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    fillRule="evenodd"
                                                    clipRule="evenodd"
                                                    d="M20.8187 14.1411C21.0344 12.6969 19.9347 11.9205 18.4311 11.4025L18.9189 9.44604L17.7276 9.14925L17.2527 11.0542C16.94 10.9761 16.6185 10.9025 16.2989 10.8296L16.7772 8.91215L15.587 8.61536L15.0989 10.5711C14.8397 10.5121 14.5853 10.4538 14.3384 10.3924L14.3398 10.3863L12.6974 9.9762L12.3806 11.2482C12.3806 11.2482 13.2642 11.4507 13.2455 11.4632C13.7279 11.5836 13.8154 11.9028 13.8004 12.1559L13.2448 14.3847C13.2781 14.3932 13.3212 14.4054 13.3686 14.4244L13.2431 14.3932L12.464 17.5154C12.405 17.662 12.2554 17.8818 11.9183 17.7983C11.9301 17.8156 11.0526 17.5823 11.0526 17.5823L10.4614 18.9458L12.0115 19.3322C12.1865 19.376 12.3594 19.4211 12.5304 19.4657L12.5304 19.4657L12.5308 19.4658C12.6414 19.4947 12.7511 19.5233 12.8602 19.5513L12.3673 21.5305L13.5569 21.8273L14.0453 19.8694C14.3699 19.9576 14.6854 20.039 14.9941 20.1157L14.5077 22.0643L15.6986 22.3611L16.1914 20.386C18.2222 20.7703 19.7495 20.6153 20.3916 18.7789C20.9096 17.3 20.3662 16.447 19.2977 15.8904C20.0758 15.7103 20.662 15.1984 20.8183 14.1411H20.8187ZM18.0973 17.9567C17.7592 19.3141 15.6342 18.7524 14.6628 18.4957L14.6627 18.4957C14.5757 18.4727 14.498 18.4522 14.4317 18.4357L15.0856 15.814C15.1668 15.8343 15.2659 15.8565 15.3781 15.8817L15.3782 15.8817C16.3832 16.1073 18.4427 16.5694 18.0977 17.9567H18.0973ZM15.5802 14.6652C16.3907 14.8814 18.1576 15.3528 18.4654 14.1198C18.78 12.8583 17.062 12.4781 16.2229 12.2925C16.1286 12.2716 16.0454 12.2532 15.9774 12.2362L15.3845 14.614C15.4406 14.628 15.5064 14.6455 15.5802 14.6652Z"
                                                    fill="#F7931A"
                                                />
                                            </svg>
                                            <span className="me-2">
                                                بیت کوین <span>(BTC)</span>
                                            </span>
                                        </td>
                                        <td className="align-middle">
                                            <span>1,102,000</span>
                                        </td>
                                        <td>
                                            <Neg>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M1 1L5 5L9 1"
                                                        stroke="#EB5757"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Neg>
                                        </td>
                                        <td>
                                            <Plus>
                                                <span>% 10</span>
                                                <svg
                                                    width="10"
                                                    height="6"
                                                    viewBox="0 0 10 6"
                                                    fill="none"
                                                    xmlns="http://www.w3.org/2000/svg"
                                                >
                                                    <path
                                                        d="M9 5L5 1L1 5"
                                                        stroke="#27AE60"
                                                        strokeWidth="1.5"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                    />
                                                </svg>
                                            </Plus>
                                        </td>
                                        <td>
                                            <Image
                                                src="/images/landing-chart.png"
                                                width={111}
                                                height={33}
                                                alt="chart"
                                            />
                                        </td>
                                        <td>
                                            <svg
                                                width="26"
                                                height="26"
                                                viewBox="0 0 26 26"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M13 6.49733C13.2762 6.49733 13.5002 6.72128 13.5002 6.99753C13.5002 7.27379 13.2762 7.49774 13 7.49774C12.7237 7.49774 12.4998 7.27379 12.4998 6.99753C12.4998 6.72128 12.7237 6.49733 13 6.49733"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 12.4998C13.2762 12.4998 13.5002 12.7238 13.5002 13C13.5002 13.2763 13.2762 13.5002 13 13.5002C12.7237 13.5002 12.4998 13.2763 12.4998 13C12.4998 12.7238 12.7237 12.4998 13 12.4998"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                                <path
                                                    d="M13 18.5023C13.2762 18.5023 13.5002 18.7262 13.5002 19.0025C13.5002 19.2787 13.2762 19.5027 13 19.5027C12.7237 19.5027 12.4998 19.2787 12.4998 19.0025C12.4998 18.7262 12.7237 18.5023 13 18.5023"
                                                    stroke="#323232"
                                                    strokeWdth="1.5"
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                />
                                            </svg>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </ChooseShop>
                </HeadMain>
            </div>
            <Blog>
                <div className="blog-text">
                    <p>وبلاگ بیت هولد</p>
                    <span>درباره اخرین اخبار رمز ارز بخوانید</span>
                </div>
                <BlogCards>
                    <div className="blog-card">
                        <Image
                            src="/images/blog.png"
                            width={270}
                            height={160}
                            alt="blog"
                        />
                        <div className="badge">اخبار</div>
                        <div className="date">15 اردیبهشت</div>
                        <div className="text">افزایش قیمت در صرافی ها ...</div>
                    </div>
                    <div className="blog-card">
                        <Image
                            src="/images/blog.png"
                            width={270}
                            height={160}
                            alt="blog"
                        />
                        <div className="badge">اخبار</div>
                        <div className="date">15 اردیبهشت</div>
                        <div className="text">افزایش قیمت در صرافی ها ...</div>
                    </div>
                    <div className="blog-card">
                        <Image
                            src="/images/blog.png"
                            width={270}
                            height={160}
                            alt="blog"
                        />
                        <div className="badge">اخبار</div>
                        <div className="date">15 اردیبهشت</div>
                        <div className="text">افزایش قیمت در صرافی ها ...</div>
                    </div>
                    <div className="blog-card">
                        <Image
                            src="/images/blog.png"
                            width={270}
                            height={160}
                            alt="blog"
                        />
                        <div className="badge">اخبار</div>
                        <div className="date">15 اردیبهشت</div>
                        <div className="text">افزایش قیمت در صرافی ها ...</div>
                    </div>
                </BlogCards>
            </Blog>
            <WhyBithold className="text-center">
                <p>چرا از بیت هولد خرید کنیم</p>
                <span>
                    بیت هولد بازار وسیع ارز دیجیتال است که دارای نشان اعتماد
                </span>
                <div className="d-flex cardss justify-content-between">
                    <div className="card">
                        <div>
                            <Image
                                src="/images/why-card-1.png"
                                alt="why-bithold"
                                width={118}
                                height={118}
                            />
                        </div>
                        <div>
                            <h6>کیف پول امن</h6>
                            <p>
                                امن ترین کیف پول برای واریز انواع رمز ارزها و
                                خرید و فروش بین رمز ارزهای دیگر
                            </p>
                        </div>
                    </div>
                    <div className="card">
                        <div>
                            <Image
                                src="/images/why-card-2.png"
                                alt="why-bithold"
                                width={118}
                                height={118}
                            />
                        </div>
                        <div>
                            <h6>کیف پول امن</h6>
                            <p>
                                امن ترین کیف پول برای واریز انواع رمز ارزها و
                                خرید و فروش بین رمز ارزهای دیگر
                            </p>
                        </div>
                    </div>
                    <div className="card">
                        <div>
                            <Image
                                src="/images/why-card-3.png"
                                alt="why-bithold"
                                width={118}
                                height={118}
                            />
                        </div>
                        <div>
                            <h6>کیف پول امن</h6>
                            <p>
                                امن ترین کیف پول برای واریز انواع رمز ارزها و
                                خرید و فروش بین رمز ارزهای دیگر
                            </p>
                        </div>
                    </div>
                </div>
            </WhyBithold>
            <Application>
                <App>
                    <h4>نصب اپلیکیشن بیت هولد</h4>
                    <p>
                        با نصب اپلیکیشن بیت هولد ، سریعتر از همیشه ترید کنید و
                        از آمار و اطلاعات بازار مطلع شوید
                    </p>
                    <div className="d-flex mt-5">
                        <div className="ms-2">
                            <Image
                                className=""
                                width={157}
                                height={47}
                                src="/images/app.png"
                                alt="app"
                            />
                        </div>
                        <div>
                            <Image
                                className=""
                                width={157}
                                height={47}
                                src="/images/app.png"
                                alt="app"
                            />
                        </div>
                    </div>
                </App>
                <Image
                    src="/images/phone.png"
                    width={344}
                    height={344}
                    alt="phones"
                />
            </Application>
            <Footer>
                <div className="d-flex">
                    <div className="footer-col-1">
                        <Image
                            alt="logo"
                            src="/images/header-logo.png"
                            width={97}
                            height={40}
                        />
                        <h5>بیت هولد</h5>
                        <p>
                            بیت هولد یک صرافی بزرگ در سال 1400 تاسیس شده که بر
                            پایه امنیت کاربران تلاش کرده است همچنین آنان با تلاش
                            بر بهبود تجربه کاربران
                        </p>
                    </div>
                    <div className="footer-col-2 text-center">
                        <h5>دسترسی سریع</h5>
                        <div className="mt-4">پیگیری سفارشات</div>
                        <div className="mt-2">درباره ما</div>
                        <div className="mt-2">تماس با ما</div>
                    </div>
                </div>
                <div className="d-flex res-mt">
                    <div className="footer-col-2 text-center">
                        <h5>خبرنامه</h5>
                        <label className="d-flex flex-column align-items-start">
                            آدرس ایمیل
                            <input type="email" />
                        </label>
                        <button>ثبت</button>
                    </div>
                    <div className="footer-col-4 text-center">
                        <h5>نماد ها</h5>
                        <Image
                            src="/images/e-namad.png"
                            width={100}
                            height={100}
                            alt="e-namad"
                        />
                        <Image
                            src="/images/e-namad.png"
                            width={100}
                            height={100}
                            alt="e-namad"
                        />
                    </div>
                </div>
            </Footer>
            <CopyRight>کلیه حقوق برای بیت هولد محفوظ میباشد 2022</CopyRight>
        </Main>
    );
}
