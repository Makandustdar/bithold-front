import { useState } from "react";
import NightModeContext from "../components/Context";
import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
    const [night, setNight] = useState();

    const setStatus = (i) => {
        setNight(i);
    };
    return (
        <NightModeContext.Provider
            value={{
                night,
                setStatus,
            }}
        >
            <Component {...pageProps} />
        </NightModeContext.Provider>
    );
}

export default MyApp;
