import Router from "next/router";
import { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import { baseUrl } from "../components/BaseUrl";
import Header from "../components/Header";
import Sidebar from "../components/Sidebar";
import "bootstrap/dist/css/bootstrap.css";
import Image from "next/image";
import axios from "axios";
import NightModeContext from "../components/Context";

const Content = styled.div`
    overflow: hidden;
    transition: 0.5s all;
    background-color: #edf8fc;
    width: 100%;
    min-height: 100vh;
    padding-bottom: 70px;
    @media (max-width: 1300px) {
        .mx-1200 {
            flex-wrap: wrap;
        }
        .y-inv {
            margin-right: 0;
        }
    }

    @media (max-width: 992px) {
        .mx-1200 {
            flex-wrap: wrap;
            flex-direction: column;
            align-items: center;
        }
        .y-inv {
            margin-right: 0;
        }
    }
    @media (max-width: 786px) {
    }
    .scrollable {
        max-height: 450px !important;
        overflow-y: auto !important;
        overflow-x: hidden;
        tbody tr {
            width: 336px;
        }
        ::-webkit-scrollbar {
            width: 5px;
            height: 9px;
        }
        ::-webkit-scrollbar-thumb {
            background-color: #00293957;
            border-radius: 20px;
            width: 5px;
        }
    }
`;
const ProfMain = styled.div`
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    padding: 70px 32px;
    overflow: hidden;
    @media (max-width: 1250px) {
        padding-top: 16px;
    }
`;
const RightBox = styled.div`
    width: 289px;
    height: 283px;
    background-color: #fff;
    box-shadow: 0px 2px 8px rgba(50, 50, 50, 0.12);
    border-radius: 16px;
    display: flex;

    flex-direction: column;
    padding: 24px;
    svg {
        margin-left: 6px;
    }
    .auth-btn {
        margin-top: 24px;
        width: 130px;
        margin-right: auto;
        margin-left: auto;
        height: 40px;
        border-radius: 8px;
    }
    .edit-prof {
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding-bottom: 20px;
        border-bottom: 1px solid #eee;
        span {
            font-size: 16px;
            font-weight: 700;
        }
    }
    .setting {
        display: flex;
        align-items: center;
        padding-bottom: 13px;
        padding-top: 13px;
        border-bottom: 1px solid #eee;
        span {
            color: #777777;
        }
    }
    @media (max-width: 1250px) {
        margin-bottom: 50px;
        margin-top: 0;
    }
`;
const LeftBox = styled.div`
    width: 711px;
    padding: 32px;
    background-color: #fff;
    box-shadow: 0px 2px 8px rgba(50, 50, 50, 0.12);
    border-radius: 16px;
    position: relative;
    .img-prof {
        position: absolute;
        top: -39px;
        left: 316px;
    }
    label {
        display: block;
        margin-right: 8px;
    }
    .w-162 {
        width: 160px;
    }
    @media (max-width: 1250px) {
        margin-top: 20px;
        .img-prof {
            left: 44%;
        }
    }
    @media (max-width: 786px) {
        width: 328px !important;
        padding: 16px;
        label {
            width: 100%;
            margin-right: 0;
        }
        .img-prof {
            left: 40%;
        }
    }
`;
const Alert = styled.div`
    width: 473px;
    height: 52px;
    background: #f6543e;
    border-radius: 8px;
    margin-left: auto;
    margin-right: auto;
    margin-top: 20px;
    margin-bottom: 20px;
    padding: 14px 10px;
    font-size: 13px;
    color: #fff;
    svg {
        margin-left: 5px;
    }
    @media (max-width: 786px) {
        width: 298px !important;
        padding: 4px;
        margin-top: 40px;
    }
`;

const Inp = styled.input`
    background: #ffffff;
    border: 1.5px solid #dbdbdb;
    border-radius: 8px;
    height: 44px;
    padding: 10px;
    margin-top: 5px;
    margin-bottom: 20px;
    width: 210px;
    margin-left: 8px;
    @media (max-width: 786px) {
        width: 100% !important;
    }
`;

const SubBtn = styled.button`
    width: 228px;
    height: 39px;
    background: linear-gradient(90deg, #128cbd -1.72%, #3dbdc8 100%);
    border-radius: 32px;
    margin-right: auto !important;
    margin-left: auto !important;
    color: #fff;
`;

export default function Profile() {
    useEffect(() => {
        if (
            localStorage.getItem("token") == null ||
            typeof window == "undefined"
        ) {
            Router.push("/login");
        }
    }, []);
    const stts = useContext(NightModeContext);
    console.log(stts);
    let refreshToken = "";
    setTimeout(() => {
        refreshToken = localStorage.getItem("refresh_token");
    }, 2000);

    setTimeout(() => {
        setInterval(() => {
            inter();
        }, 600000);
    }, 70000);
    const inter = () => {
        let data = {
            refresh: refreshToken,
        };
        let config = {
            method: "POST",
            url: `${baseUrl}token/refresh/`,
            data: data,
        };

        axios(config)
            .then((response) => {
                localStorage.setItem("token", response.data.access);
            })
            .catch((error) => {});
    };
    const [showMenu, setShowMenu] = useState(true);
    const menuHandler = () => {
        setShowMenu(!showMenu);
    };

    return (
        <div className="max-w-1992">
            <Sidebar show-menu={menuHandler} active="5" show={showMenu} />
            <Content
                className={
                    showMenu
                        ? stts.night == "true"
                            ? "pr-176 bg-dark-2"
                            : "pr-176 "
                        : stts.night == "true"
                        ? "bg-dark-2"
                        : ""
                }
            >
                <Header show-menu={menuHandler} />
                <ProfMain>
                    <RightBox className={stts.night == "true" ? "bg-gray" : ""}>
                        <div className="edit-prof d-flex align-items-center">
                            <div className="d-flex align-items-center">
                                <svg
                                    className={
                                        stts.night == "true" ? "svg-white" : ""
                                    }
                                    width="32"
                                    height="32"
                                    viewBox="0 0 32 32"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        fillRule="evenodd"
                                        clipRule="evenodd"
                                        d="M7.05732 20.8253L20.8253 7.05733C21.3453 6.53733 22.1893 6.53733 22.7093 7.05733L24.944 9.29199C25.464 9.81199 25.464 10.656 24.944 11.176L11.1747 24.9427C10.9253 25.1933 10.5867 25.3333 10.2333 25.3333H6.66666V21.7667C6.66666 21.4133 6.80666 21.0747 7.05732 20.8253Z"
                                        stroke="#323232"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                    <path
                                        d="M18.3333 9.54666L22.4533 13.6667"
                                        stroke="#323232"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                </svg>
                                <span>ویرایش پروفایل</span>
                            </div>
                            <svg
                                className={
                                    stts.night == "true" ? "svg-white" : ""
                                }
                                width="8"
                                height="14"
                                viewBox="0 0 8 14"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M6.66666 1.66667L1.33333 7.00001L6.66666 12.3333"
                                    stroke="#323232"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                            </svg>
                        </div>
                        <div className="setting c-p">
                            <svg
                                width="32"
                                height="32"
                                viewBox="0 0 32 32"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M18.8284 13.1716C20.3905 14.7337 20.3905 17.2663 18.8284 18.8284C17.2663 20.3905 14.7336 20.3905 13.1715 18.8284C11.6094 17.2663 11.6094 14.7337 13.1715 13.1716C14.7336 11.6095 17.2663 11.6095 18.8284 13.1716"
                                    stroke="#777777"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                                <path
                                    fillRule="evenodd"
                                    clipRule="evenodd"
                                    d="M21.5733 24.9667V24.9667C22.244 25.6373 23.3333 25.6373 24.004 24.9667L24.9667 24.004C25.6373 23.3333 25.6373 22.244 24.9667 21.5733V21.5733C24.4573 21.064 24.308 20.3 24.588 19.636C24.6173 19.5653 24.6467 19.4947 24.6747 19.4227C24.9187 18.8013 25.524 18.4013 26.1907 18.4013H26.28C27.2293 18.4013 27.9987 17.632 27.9987 16.6827V15.3213C27.9987 14.372 27.2293 13.6027 26.28 13.6027H26.1907C25.524 13.6027 24.9187 13.2013 24.6747 12.5813C24.6467 12.5093 24.6173 12.4387 24.588 12.368C24.308 11.704 24.4573 10.94 24.9667 10.4307V10.4307C25.6373 9.76 25.6373 8.67067 24.9667 8L24.004 7.03733C23.3333 6.36667 22.244 6.36667 21.5733 7.03733V7.03733C21.064 7.54667 20.3 7.696 19.636 7.416C19.5653 7.38667 19.4947 7.35733 19.4227 7.32933C18.8013 7.08133 18.4 6.47467 18.4 5.808V5.71867C18.4 4.76933 17.6307 4 16.6813 4H15.32C14.3693 4 13.6 4.76933 13.6 5.71867V5.808C13.6 6.47467 13.1987 7.08 12.5787 7.324C12.5067 7.35333 12.436 7.38133 12.3653 7.412C11.7013 7.692 10.9373 7.54267 10.428 7.03333V7.03333C9.75733 6.36267 8.668 6.36267 7.99733 7.03333L7.03333 7.996C6.36267 8.66667 6.36267 9.756 7.03333 10.4267V10.4267C7.54267 10.936 7.692 11.7 7.412 12.364C7.38133 12.436 7.35333 12.5067 7.32533 12.5787C7.08133 13.1987 6.47467 13.6 5.808 13.6H5.71867C4.76933 13.6 4 14.3693 4 15.3187V16.68C4 17.6307 4.76933 18.4 5.71867 18.4H5.808C6.47467 18.4 7.08 18.8013 7.324 19.4213C7.352 19.4933 7.38133 19.564 7.41067 19.6347C7.69067 20.2987 7.54133 21.0627 7.032 21.572V21.572C6.36133 22.2427 6.36133 23.332 7.032 24.0027L7.99467 24.9653C8.66533 25.636 9.75467 25.636 10.4253 24.9653V24.9653C10.9347 24.456 11.6987 24.3067 12.3627 24.5867C12.4333 24.616 12.504 24.6453 12.576 24.6733C13.1973 24.9173 13.5973 25.5227 13.5973 26.1893V26.2787C13.5973 27.228 14.3667 27.9973 15.316 27.9973H16.6773C17.6267 27.9973 18.396 27.228 18.396 26.2787V26.1893C18.396 25.5227 18.7973 24.9173 19.4173 24.6733C19.4893 24.6453 19.56 24.616 19.6307 24.5867C20.2987 24.308 21.0627 24.4573 21.5733 24.9667V24.9667Z"
                                    stroke="#777777"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                            </svg>
                            <span>تنظیمات</span>
                        </div>
                        <div className="setting c-p">
                            <svg
                                width="34"
                                height="34"
                                viewBox="0 0 34 34"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M14.512 16.8199C14.5847 16.8926 14.6065 17.002 14.5671 17.097C14.5278 17.192 14.4351 17.254 14.3322 17.254C14.2294 17.254 14.1367 17.192 14.0973 17.097C14.058 17.002 14.0797 16.8926 14.1524 16.8199C14.2518 16.7209 14.4126 16.7209 14.512 16.8199"
                                    stroke="#777777"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                                <path
                                    d="M15.6661 23.6694H6.32891C5.62131 23.6696 4.94264 23.3886 4.44229 22.8883C3.94194 22.3879 3.66094 21.7093 3.66113 21.0017V12.9983C3.66094 12.2907 3.94194 11.6121 4.44229 11.1117C4.94264 10.6114 5.62131 10.3304 6.32891 10.3306H15.6661"
                                    stroke="#777777"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                                <path
                                    d="M9.17647 16.8199C9.24919 16.8926 9.27094 17.002 9.23159 17.097C9.19223 17.192 9.09952 17.254 8.99669 17.254C8.89385 17.254 8.80114 17.192 8.76179 17.097C8.72243 17.002 8.74419 16.8926 8.81691 16.8199C8.9163 16.7209 9.07708 16.7209 9.17647 16.8199"
                                    stroke="#777777"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                                <rect
                                    x="19.6678"
                                    y="14.9992"
                                    width="10.6711"
                                    height="8.67028"
                                    rx="1.65"
                                    stroke="#777777"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                                <path
                                    d="M21.7079 14.9992V12.2922C21.7079 10.4721 23.1833 8.99667 25.0033 8.99667V8.99667C25.8774 8.99667 26.7156 9.34387 27.3336 9.96189C27.9516 10.5799 28.2988 11.4181 28.2988 12.2922V12.2922V15.0217"
                                    stroke="#777777"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                            </svg>

                            <span>امنیت</span>
                        </div>
                        <button
                            onClick={() => {
                                Router.push("/auth");
                            }}
                            className="auth-btn btn-warning"
                        >
                            احراز هویت
                        </button>
                    </RightBox>
                    <LeftBox className={stts.night == "true" ? "bg-gray" : ""}>
                        <img
                            className="img-prof"
                            src="/images/prof-img.png"
                            width={78}
                            height={78}
                            alt="profile"
                        />
                        <Alert>
                            <svg
                                width="25"
                                height="24"
                                viewBox="0 0 25 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <rect
                                    width="24"
                                    height="24"
                                    transform="matrix(-1 0 0 1 24.5 0)"
                                    fill="white"
                                    fillOpacity="0.01"
                                />
                                <path
                                    fillRule="evenodd"
                                    clipRule="evenodd"
                                    d="M13.438 4.967C12.922 3.993 12.08 3.989 11.562 4.967L4.438 18.425C3.922 19.399 4.393 20.196 5.495 20.196H19.505C20.607 20.196 21.08 19.403 20.562 18.425L13.438 4.967ZM13.2071 14.7071C13.0196 14.8946 12.7652 15 12.5 15C12.2348 15 11.9804 14.8946 11.7929 14.7071C11.6054 14.5196 11.5 14.2652 11.5 14V9C11.5 8.73478 11.6054 8.48043 11.7929 8.29289C11.9804 8.10536 12.2348 8 12.5 8C12.7652 8 13.0196 8.10536 13.2071 8.29289C13.3946 8.48043 13.5 8.73478 13.5 9V14C13.5 14.2652 13.3946 14.5196 13.2071 14.7071ZM13.2071 17.7071C13.0196 17.8946 12.7652 18 12.5 18C12.2348 18 11.9804 17.8946 11.7929 17.7071C11.6054 17.5196 11.5 17.2652 11.5 17C11.5 16.7348 11.6054 16.4804 11.7929 16.2929C11.9804 16.1054 12.2348 16 12.5 16C12.7652 16 13.0196 16.1054 13.2071 16.2929C13.3946 16.4804 13.5 16.7348 13.5 17C13.5 17.2652 13.3946 17.5196 13.2071 17.7071Z"
                                    fill="white"
                                />
                            </svg>
                            جهت احراز هویت اطلاعات حساب کاربری خود را همراه با
                            مدارک بارگذاری کنید.
                        </Alert>
                        <div className="d-flex flex-wrap justify-content-center">
                            <label>
                                <div>نام و نام خانوادگی</div>
                                <Inp />
                            </label>
                            <label>
                                <div>نام پدر</div>
                                <Inp className="w-162" />
                            </label>
                            <label>
                                <div>شماره همراه</div>
                                <Inp type="number" />
                            </label>
                            <label>
                                <div>کد ملی</div>
                                <Inp />
                            </label>
                            <label>
                                <div>شماره ثابت</div>
                                <Inp className="w-162" type="number" />
                            </label>
                            <label>
                                <div>کد پستی</div>
                                <Inp />
                            </label>
                        </div>

                        <label>
                            <div>آدرس</div>
                            <Inp
                                className="w-100"
                                placeholder="آدرس کامل محل سکونت"
                            />
                        </label>
                        <div className="w-100 d-flex justify-content-center mt-3">
                            <SubBtn>ذخیره و بررسی اطلاعات</SubBtn>
                        </div>
                    </LeftBox>
                </ProfMain>
            </Content>
        </div>
    );
}
