import Image from "next/image";
import { useEffect, useState } from "react";
import styled from "styled-components";
import "bootstrap/dist/css/bootstrap.css";
import Router from "next/router";
import ReactCodeInput from "react-code-input";
import axios from "axios";
import { baseUrl } from "../components/BaseUrl";
import { ToastContainer, toast } from "react-toastify";
import Head from "next/head";
import "react-toastify/dist/ReactToastify.css";

const Main = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: #edf8fc;
    padding: 24px;
`;

const Content = styled.div`
    .w-50 {
        width: 100%;
    }
    max-width: 1280px;
    display: flex;
    justify-content: space-between;
    width: 100%;
    @media (max-width: 992px) {
        .w-50 {
            display: none !important;
        }
        justify-content: center;
    }
`;

const LeftContent = styled.div`
    align-items: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 50%;
`;

const Box = styled.div`
    width: 400px;
    height: 352px;
    background: #ffffff;
    box-shadow: 0px 2px 8px rgba(50, 50, 50, 0.12);
    border-radius: 16px;
    margin-top: 32px;
    padding: 16px;
    h4 {
        font-weight: 600;
        font-size: 14px;
        margin-top: 20px;
        line-height: 20px;
        color: #323232;
    }
    label {
        display: flex;
        flex-direction: column;
        margin-top: 36px;
        margin-bottom: 8px;
        font-size: 16px;
        line-height: 23px;
    }
    input {
        margin-top: 8px;
        background-color: #edf8fc;
        width: 100%;
        height: 44px;
        border: 1.5px solid #dbdbdb;
        box-sizing: border-box;
        border-radius: 8px;
        padding: 10px;
    }
    .l-t-r {
        direction: ltr;
    }
    .resend {
        color: #2d3bfa;
    }
    @media (max-width: 992px) {
        width: 343px;
    }
`;

const BoxHead = styled.div`
    width: 163px;
    height: 34px;
    background-color: #edf8fc;
    border-radius: 6px;
    padding: 4px;
    button {
        width: 75px;
        height: 100%;
    }
    .login {
        background: #108abb;
        color: #fff;
        border-radius: 10px 4px 4px 10px;
    }
    .register {
        background: #108abb;
        color: #fff;
        border-radius: 4px 10px 10px 4px;
    }
`;

const Submit = styled.button`
    width: 188px;
    height: 38px;
    background: linear-gradient(90deg, #128cbd -1.72%, #3dbdc8 100%);
    border-radius: 32px;
    color: #fff;
    transition: 0.3s all;
    :hover {
        opacity: 0.83;
    }
`;

export default function Register() {
    const [activeTab, setActiveTab] = useState("log");
    const [counter, setCounter] = useState(60);
    const [code, setCode] = useState("");
    const id =
        typeof window !== "undefined" ? localStorage.getItem("id") : null;
    const mobile =
        typeof window !== "undefined" ? localStorage.getItem("mobile") : null;
    useEffect(() => {
        const timer =
            counter > 0 && setInterval(() => setCounter(counter - 1), 1000);
        return () => clearInterval(timer);
    }, [counter]);
    const subHandler = async (e) => {
        e.preventDefault();
        let data = {
            id: id,
            otp: code,
        };
        let config = {
            method: "POST",
            url: `${baseUrl}token/verify/`,
            data: data,
        };

        await axios(config)
            .then((response) => {
                if (response.status == 200) {
                    localStorage.setItem("token", response.data.access);
                    localStorage.setItem(
                        "refresh_token",
                        response.data.refresh
                    );
                    Router.push("/dashboard");
                } else {
                    alert(response.data.message);
                }
            })
            .catch((error) => {
                toast.error(" کد وارد شده اشتباه است", {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            });
    };
    const resend = async (e) => {
        e.preventDefault();
        let data = {
            mobile,
        };
        let config = {
            method: "POST",
            url: `${baseUrl}token/otp/resend/`,
            data: data,
        };
        await axios(config)
            .then((response) => {
                if (response.status == 200) {
                    setCounter(60);
                } else {
                    alert(response.data.message);
                }
            })
            .catch((error) => {});
    };
    const handlePinChange = (pinCode) => {
        setCode(pinCode);
    };
    return (
        <Main>
            <ToastContainer
                rtl={true}
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                pauseOnFocusLoss={false}
                draggable
                pauseOnHover={false}
            />
            <Head>
                <title>Verify</title>
            </Head>
            <Content>
                <div className="w-50">
                    <Image
                        src="/images/login.png"
                        width={600}
                        height={672}
                        alt="side"
                    />
                </div>
                <LeftContent>
                    <Image
                        src="/images/mob-logo.svg"
                        width={153}
                        height={64}
                        alt="logo"
                    />
                    <Box>
                        <BoxHead>
                            <button
                                onClick={() => {
                                    setActiveTab("log");
                                }}
                                className={activeTab === "log" ? "login" : ""}
                                type="button"
                            >
                                ورود
                            </button>
                            <button
                                onClick={() => {
                                    setActiveTab("reg");
                                    Router.push("/register");
                                }}
                                className={
                                    activeTab === "reg" ? "register" : ""
                                }
                                type="button"
                            >
                                ثبت نام
                            </button>
                        </BoxHead>
                        <h4>ورود به حساب کاربری</h4>
                        <div className="d-flex justify-content-center text-center">
                            <label className="l-t-r">
                                کد تایید
                                <ReactCodeInput
                                    onChange={handlePinChange}
                                    type="number"
                                    fields={5}
                                />
                            </label>
                        </div>
                        {counter !== 0 ? (
                            <div className="text-center text-danger">
                                ارسال دوباره کد تا
                                <span className="mx-2">{counter}</span>
                                ثانیه دیگر
                            </div>
                        ) : (
                            <div
                                onClick={resend}
                                className="resend text-center c-p"
                            >
                                ارسال مجدد کد
                            </div>
                        )}
                        <div className="d-flex l-t-r justify-content-center mt-3">
                            <Submit onClick={subHandler} className="mt-4">
                                تایید
                            </Submit>
                        </div>
                    </Box>
                </LeftContent>
            </Content>
        </Main>
    );
}
