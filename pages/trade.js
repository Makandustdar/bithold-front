import Head from "next/head";
import "bootstrap/dist/css/bootstrap.css";
import Sidebar from "../components/Sidebar";
import styled from "styled-components";
import Header from "../components/Header";
import { useContext, useEffect, useState } from "react";
import Router from "next/router";
import axios from "axios";
import { baseUrl } from "../components/BaseUrl";
import NightModeContext from "../components/Context";

const Main = styled.div`
    background-color: #edf8fc;
    width: 100%;
    min-height: 100vh;
`;
const Content = styled.div`
    overflow: hidden;
    transition: 0.5s all;
    padding-bottom: 70px;
    @media (max-width: 1300px) {
        .mx-1200 {
            flex-wrap: wrap;
        }
        .y-inv {
            margin-right: 0;
        }
    }

    @media (max-width: 992px) {
        .mx-1200 {
            flex-wrap: wrap;
            flex-direction: column;
            align-items: center;
        }
        .y-inv {
            margin-right: 0;
        }
    }
    @media (max-width: 786px) {
    }
`;
const TradeMain = styled.div`
    padding: 32px;
    max-width: 1992px;
    width: 100%;
    margin-right: auto;
    margin-left: auto;
    iframe {
        width: 100%;
        height: 334px;
        border-radius: 16px;
        border: none !important;
    }
    @media (min-height: 700px) {
        iframe {
            height: 534px;
        }
    }
`;

const TradeBox = styled.div`
    width: 351px;
    height: 250px;
    background: #ffffff;
    box-shadow: 0px 2px 8px rgba(50, 50, 50, 0.12);
    border-radius: 16px;
    .box-head {
        width: 351px;
        height: 39px;
        border-radius: 16px 16px 0px 0px;
        padding: 8px 16px;
        font-weight: 600;
        font-size: 16px;
    }
    .buy-head {
        background: rgba(48, 224, 161, 0.2);
        color: #30e0a1;
    }
    .sell-head {
        background: rgba(246, 84, 62, 0.2);
        color: #f6543e;
    }
    .box-content {
        padding: 16px;
    }
    .border-b {
        padding-bottom: 14px;
        border-bottom: 1px solid #b3b3b3;
    }
    input {
        width: 151px;
        height: 38px;
        background: #edf8fc;
        border: 1px solid #dedede;
        box-sizing: border-box;
        border-radius: 8px;
        padding: 8px;
    }
    .dir-left {
        direction: ltr;
    }
    h5 {
        color: #323232;
        font-weight: normal;
        margin-top: 3px;
        font-size: 16px;
        line-height: 16px;
    }
    button {
        width: 319px;
        height: 42px;
        background: #30e0a1;
        border-radius: 8px;
        margin-top: 16px;
        font-weight: 600;
        font-size: 16px;
        color: #fff;
    }
    .sell-btn {
        background: #f6543e;
    }
`;

export default function Dashboard() {
    const stts = useContext(NightModeContext);
    useEffect(() => {
        if (
            localStorage.getItem("token") == null ||
            typeof window == "undefined"
        ) {
            Router.push("/login");
        }
    }, []);
    const [showMenu, setShowMenu] = useState(true);
    const menuHandler = () => {
        setShowMenu(!showMenu);
    };
    let refreshToken = "";
    setTimeout(() => {
        refreshToken = localStorage.getItem("refresh_token");
    }, 2000);

    setTimeout(() => {
        setInterval(() => {
            inter();
        }, 600000);
    }, 10000);
    const inter = () => {
        let data = {
            refresh: refreshToken,
        };
        let config = {
            method: "POST",
            url: `${baseUrl}token/refresh/`,
            data: data,
        };

        axios(config)
            .then((response) => {
                localStorage.setItem("token", response.data.access);
            })
            .catch((error) => {});
    };
    let chartUrl = `https://s.tradingview.com/widgetembed/?frameElementId=tradingview_ae654&amp;symbol=BTCUSDT&amp;interval=1H&amp;symboledit=1&amp;saveimage=1&amp;toolbarbg=f1f3f6&amp;studies=%5B%5D&amp;theme=${stts.night == "true" ? "DARK" : ""}&amp;style=1&amp;timezone=Asia%2FTehran&amp;hidevolume=1&amp;studies_overrides=%7B%7D&amp;overrides=%7B%7D&amp;enabled_features=%5B%5D&amp;disabled_features=%5B%5D&amp;locale=fa_IR&amp;utm_source=fasttrading.xyz&amp;utm_medium=widget&amp;utm_campaign=chart&amp;utm_term=BTCUSDT`;
    return (
        <Main className={stts.night == "true" ? "bg-dark-2 max-w-1992": "max-w-1992"}>
            <Head>
                <title>Trade</title>
            </Head>
            <Sidebar show-menu={menuHandler} active="2" show={showMenu} />
            <Content className={showMenu ? "pr-176" : ""}>
                <Header show-menu={menuHandler} />
                <TradeMain>
                    <div className="d-flex justify-content-between">
                        <iframe
                            id="tradingview_ae654"
                            src={chartUrl}
                            allowtransparency="true"
                            scrolling="no"
                        ></iframe>
                    </div>
                </TradeMain>
                <div className="d-flex w-100 justify-content-around">
                    <TradeBox className={stts.night == "true" ? "bg-gray" : ""}>
                        <div className="box-head buy-head">خرید</div>
                        <div className="box-content">
                            <div className="d-flex border-b justify-content-between">
                                <span>موجودی شما : </span>
                                <span>121,300,000 ریال</span>
                            </div>
                            <div className="d-flex justify-content-between mt-3">
                                <div>
                                    <h5>قیمت</h5>
                                    <input
                                        type="text"
                                        placeholder="تتر"
                                        name="price"
                                    />
                                </div>
                                <div>
                                    <h5>مقدار</h5>
                                    <input
                                        className="dir-left"
                                        type="text"
                                        placeholder="BTC"
                                        name="price"
                                    />
                                </div>
                            </div>
                            <button>خرید بیت کوین</button>
                        </div>
                    </TradeBox>
                    <TradeBox className={stts.night == "true" ? "bg-gray" : ""}>
                        <div className="box-head sell-head">فروش</div>
                        <div className="box-content">
                            <div className="d-flex border-b justify-content-between">
                                <span>موجودی شما : </span>
                                <span>121,300,000 ریال</span>
                            </div>
                            <div className="d-flex justify-content-between mt-3">
                                <div>
                                    <h5>قیمت</h5>
                                    <input
                                        type="text"
                                        placeholder="تتر"
                                        name="price"
                                    />
                                </div>
                                <div>
                                    <h5>مقدار</h5>
                                    <input
                                        className="dir-left"
                                        type="text"
                                        placeholder="BTC"
                                        name="price"
                                    />
                                </div>
                            </div>
                            <button className="sell-btn">فروش بیت کوین</button>
                        </div>
                    </TradeBox>
                </div>
            </Content>
        </Main>
    );
}
