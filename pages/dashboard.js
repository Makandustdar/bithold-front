import Head from "next/head";
import "bootstrap/dist/css/bootstrap.css";
import Sidebar from "../components/Sidebar";
import styled from "styled-components";
import Header from "../components/Header";
import { useContext, useEffect, useState } from "react";
import FastOrder from "../components/Dashboard/FastOrder";
import Router from "next/router";
import Select from "react-select";
import { baseUrl } from "../components/BaseUrl";
import axios from "axios";
import Image from "next/image";
import MyChart from "../components/Dashboard/MyChart";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import NightModeContext from "../components/Context";

const Main = styled.div`
    background-color: #edf8fc;
    width: 100%;
    min-height: 100vh;
`;
const Content = styled.div`
    overflow: hidden;
    transition: 0.5s all;
    padding-bottom: 70px;
    @media (max-width: 1300px) {
        .mx-1200 {
            flex-wrap: wrap;
        }
        .y-inv {
            margin-right: 0;
        }
    }

    @media (max-width: 992px) {
        .mx-1200 {
            flex-wrap: wrap;
            flex-direction: column;
            align-items: center;
        }
        .y-inv {
            margin-right: 0;
        }
    }
`;

const MainCoin = styled.div`
    h2 {
        font-size: 18px;
        font-weight: 600;
        margin-bottom: 12px;
    }
    padding-top: 32px;
    padding-right: 32px;
    @media (max-width: 786px) {
    }
`;

const Cards = styled.div`
    display: flex;
    align-items: center;
    overflow: auto;
    scrollbar-width: thin;
    scrollbar-color: blue orange;
    padding-bottom: 20px;
    max-width: 2000px;
    margin-right: auto;
    margin-left: auto;
    ::-webkit-scrollbar {
        width: 5px;
        height: 9px;
    }
    ::-webkit-scrollbar-thumb {
        background-color: #00293957;
        border-radius: 20px;
        width: 5px;
    }
`;

const Card = styled.div`
    position: relative;
    min-width: 187px;
    max-width: 187px;
    height: 177px;
    background-color: #fff;
    box-shadow: 0px 2px 8px rgba(50, 50, 50, 0.12);
    border-radius: 16px;
    padding: 16px;
    margin-left: 16px;
    cursor: pointer;
    h6 {
        font-weight: 600;
        font-size: 14px;
        line-height: 20px;
        margin-bottom: 0;
        margin-right: 8px;
    }
    span {
        font-weight: 400;
        font-size: 10px;
        display: block;
        margin-right: 8px;
    }
    .mt-90 {
        margin-top: 90px;
    }
    .zero {
        direction: ltr;
        width: 40px;
        height: 22px;
        border-radius: 51px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 12px;
        line-height: 17px;
        background: #a7a7a7;
        color: #000000;
        font-family: serif !important;
        letter-spacing: 1.2px;
        font-weight: 600;
    }
    .red {
        direction: ltr;
        width: 40px;
        height: 22px;
        border-radius: 51px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 12px;
        line-height: 17px;
        background: rgba(246, 84, 62, 0.2);
        color: #f6543e;
        font-family: serif !important;
        letter-spacing: 1.2px;
        font-weight: 600;
    }
    .green {
        direction: ltr;
        width: 40px;
        height: 22px;
        border-radius: 51px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 12px;
        line-height: 17px;
        background: rgba(48, 224, 161, 0.2);
        color: #30e0a1;
        font-family: serif !important;
        letter-spacing: 1.2px;
        font-weight: 600;
    }
    .price {
        color: #323232;
        font-size: 12px;
        line-height: 17px;
    }
`;

const LastOrders = styled.div`
    white-space: nowrap;
    margin-top: 32px;
    .scrollable {
        max-height: 227px !important;
        min-height: 227px !important;
        overflow-y: auto !important;
        overflow-x: hidden;
        width: 250px;
        tbody tr {
            width: 250px !important;
        }
        ::-webkit-scrollbar {
            width: 5px;
            height: 9px;
        }
        ::-webkit-scrollbar-thumb {
            background-color: #00293957;
            border-radius: 20px;
            width: 5px;
        }
    }
    .last-box {
        background: #ffffff;
        box-shadow: 0px 2px 8px rgba(50, 50, 50, 0.12);
        border-radius: 16px;
        width: 250px;
        margin-top: 16px;
        padding: 20px;
        tbody {
            overflow-y: auto !important;
            max-height: 280px !important;
        }
        thead tr,
        tbody tr {
            display: flex;
        }
        thead th {
            padding: 16px !important;
            font-weight: 400;
            color: #657d95;
            font-size: 14px;
            line-height: 20px;
            width: 100%;
        }
        tbody td {
            padding: 8px 20px;
            font-weight: normal;
            font-size: 14px;
            line-height: 20px;
        }
        tbody > tr:nth-of-type(even) > * {
            width: 100%;
        }
    }
    .arrows {
        display: flex;
        flex-direction: column;
        margin-left: 3px;
        svg {
            margin-bottom: 3px;
        }
    }
    @media (max-width: 992px) {
        .last-box,
        .scrollable tbody tr,
        .scrollable {
            width: 343px !important;
        }
    }
`;
const YourInventory = styled.div`
    white-space: nowrap;
    margin-top: 32px;
    margin-right: 32px;
    .scrollable {
        max-height: 227px !important;
        min-height: 227px !important;
        overflow-y: auto !important;
        overflow-x: hidden;
        overflow-x: hidden;
        tbody tr {
            width: 336px;
        }
        ::-webkit-scrollbar {
            width: 5px;
            height: 9px;
        }
        ::-webkit-scrollbar-thumb {
            background-color: #00293957;
            border-radius: 20px;
            width: 5px;
        }
    }
    .last-box {
        background: #ffffff;
        box-shadow: 0px 2px 8px rgba(50, 50, 50, 0.12);
        border-radius: 16px;
        width: 340px;
        margin-top: 16px;
        padding: 20px;
        tbody {
            overflow-y: auto !important;
            max-height: 280px !important;
        }
        thead tr,
        tbody tr {
            display: flex;
        }
        thead th {
            padding: 16px !important;
            font-weight: 400;
            color: #657d95;
            font-size: 14px;
            line-height: 20px;
            width: 100%;
        }
        tbody td {
            padding: 8px 20px;
            font-weight: normal;
            font-size: 14px;
            line-height: 20px;
        }
        tbody > tr:nth-of-type(even) > * {
            width: 100%;
        }
    }
    .arrows {
        display: flex;
        flex-direction: column;
        margin-left: 3px;
        svg {
            margin-bottom: 3px;
        }
    }
`;

const Change = styled.div`
    h2 {
        margin-bottom: 67px;
    }
    .change {
        width: 336px;
        height: 283px;
        background: #ffffff;
        box-shadow: 0px 2px 8px rgba(50, 50, 50, 0.12);
        border-radius: 16px;
        position: relative;
    }
    .change-head {
        background: rgba(255, 157, 0, 0.2);
        width: 336px;
        height: 39px;
        border-radius: 16px 16px 0px 0px;
        padding: 8px 16px;
        color: #ff9d00;
        font-weight: 600;
        font-size: 16px;
    }
    .change-svg {
        position: absolute;
        left: 20px;
        top: 20px;
    }
    .padd {
        padding: 16px;
    }
    .send-coin {
        color: #4c4c4c;
        font-weight: normal;
        font-size: 16px;
        line-height: 23px;
    }
    .send-box {
        display: flex;
        border-radius: 8px;
        border: 1.5px solid #dbdbdb;
        img {
            width: 22px;
            margin-left: 10px;
        }
    }
    .css-1s2u09g-control,
    .css-b62m3t-container {
        border: none !important;
        border-radius: 0 !important;
        border-top-left-radius: 8px !important;
        border-bottom-left-radius: 8px !important;
    }
    .css-b62m3t-container {
        border-right: 1px solid #dbdbdb !important;
    }

    .css-319lph-ValueContainer {
        border: none !important;
        border-radius: 0 !important;
    }

    .css-14el2xx-placeholder {
        font-size: 14px;
    }
    .amount {
        width: 152px;
        font-size: 14px;
        background: #edf8fc;
        border-left: 1px solid #dbdbdb;
        border-radius: 0 8px 8px 0;
        padding: 8px;
    }
    button {
        margin-top: 20px;
        color: #fff;
        width: 304px;
        height: 42px;
        background: #ff9d00;
        border-radius: 8px;
    }
`;

export default function Dashboard() {
    const [moedas, setMoedas] = useState([]);
    const [sourcePrice, setSourcePrice] = useState();
    const [changed, setChanged] = useState("source");
    const [orderList, setOrderList] = useState([]);
    const [coins, setCoins] = useState([]);
    const stts = useContext(NightModeContext);
    let token = "";
    setTimeout(() => {
        token = localStorage.getItem("token");
    }, 2000);
    let refreshToken = "";
    setTimeout(() => {
        refreshToken = localStorage.getItem("refresh_token");
    }, 10000);
    
    setTimeout(() => {
        setInterval(() => {
            inter()
        }, 600000);
    }, 70000);
    const inter = () => {
        let data = {
            refresh: refreshToken,
        };
        let config = {
            method: "POST",
            url: `${baseUrl}token/refresh/`,
            data: data,
        };

        axios(config)
            .then((response) => {
                localStorage.setItem("token", response.data.access);
            })
            .catch((error) => {
            });
    }
    useEffect(() => {
        axios
            .get(
                "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=20&page=1&sparkline=false"
            )
            .then((res) => {
                setMoedas(res.data);
            })
            .catch((error) => console.log(error));
    }, []);

    const nightMode =
        typeof window !== "undefined" ? localStorage.getItem("night") : null;
    useEffect(() => {
        if (
            localStorage.getItem("token") == null ||
            typeof window == "undefined"
        ) {
            Router.push("/login");
        }
    }, []);
    let config = {
        url: `${baseUrl}service/list/`,
        method: "GET",
    };
    useEffect(() => {
        axios(config)
            .then((res) => {
                if (res.status == "200") {
                    setCoins(res.data);
                }
            })
            .catch((error) => {});
    }, []);
    let order_config = {};
    setTimeout(() => {
        order_config = {
            headers: {
                "Content-type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            url: `${baseUrl}order/list/`,
            method: "GET",
        };
    }, 3000);
    useEffect(() => {
        setTimeout(() => {
            axios(order_config)
                .then((res) => {
                    if (res.status == "200") {
                        setOrderList(res.data);
                    }
                })
                .catch((error) => {});
        }, 3200);
    }, []);

    const [showMenu, setShowMenu] = useState(true);
    const [selectedOption, setSelectedOption] = useState(null);
    const [selectedOptionTwo, setSelectedOptionTwo] = useState(null);

    const menuHandler = () => {
        setShowMenu(!showMenu);
    };
    const handleChange = (selectedOption) => {
        setSelectedOption(selectedOption);
    };
    const handleChangeTwo = (selectedOptionTwo) => {
        setSelectedOptionTwo(selectedOptionTwo);
    };

    const submitChangeCoinHandler = async (e) => {
        e.preventDefault();
        let data = {
            source: selectedOptionTwo.value,
            destination: selectedOption.value,
            changed: "source",
            "source-price": sourcePrice,
            "destination-price": 0,
        };
        let config = {
            headers: {
                "Content-type": "application/json",
                Authorization: `Bearer ${token}`,
            },
            method: "POST",
            url: `${baseUrl}order/calculator/`,
            data: data,
        };

        await axios(config)
            .then((response) => {
            })
            .catch((error) => {
                toast.error("خطایی وجود دارد", {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            });
    };

    return (
        <Main className={stts.night == "true" ? "bg-dark-2 max-w-1992": "max-w-1992"}>
            <Head>
                <title>Dashboard</title>
            </Head>
            <ToastContainer
                rtl={true}
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                pauseOnFocusLoss={false}
                draggable
                pauseOnHover={false}
            />
            <Sidebar show-menu={menuHandler} active="1" show={showMenu} />
            <Content className={showMenu ? "pr-176" : ""}>
                <Header show-menu={menuHandler} />
                <MainCoin>
                    <h2 className={stts.night == "true" ? "color-white-2 ": ""}>ارز های اصلی</h2>
                    <Cards>
                        {moedas.map((item) => {
                            return (
                                <Card  className={stts.night == "true" ? "bg-gray ": ""} key={item.id}>
                                    <div className="d-flex">
                                        <img
                                            src={item.image}
                                            alt="coin"
                                            width={36}
                                            height={36}
                                        />
                                        <div className="name">
                                            <h6>{item.id}</h6>
                                            <span>
                                                {item.symbol.toUpperCase()}
                                            </span>
                                        </div>
                                    </div>
                                    <MyChart
                                        id={item.id}
                                        strokee={
                                            item.market_cap_change_percentage_24h >
                                            0
                                                ? ["#30E0A1"]
                                                : ["#F6543E"]
                                        }
                                    />
                                    <div className="d-flex align-items-center mt-90">
                                        <div
                                            className={
                                                item.market_cap_change_percentage_24h ==
                                                0
                                                    ? "zero"
                                                    : item.market_cap_change_percentage_24h >
                                                      0
                                                    ? "green"
                                                    : "red"
                                            }
                                        >
                                            {item.market_cap_change_percentage_24h.toFixed(
                                                2
                                            )}
                                        </div>
                                        <div className={stts.night == "true" ? "color-white-2 price me-2 ": " price me-2"}>
                                            {item.current_price} $
                                        </div>
                                    </div>
                                </Card>
                            );
                        })}
                    </Cards>
                </MainCoin>
                <div className="d-flex mx-1200 justify-content-between px-32 pb-100">
                    <FastOrder night={stts.night} coins={coins} />
                    <Change>
                        <h2> </h2>
                        <div  className={stts.night == "true" ? "bg-gray change": "change"}>
                            <div className="change-head">تبدیل</div>
                            <svg
                                className="change-svg"
                                width="40"
                                height="40"
                                viewBox="0 0 40 40"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <circle
                                    cx="20"
                                    cy="20"
                                    r="19"
                                    fill="#FF9D00"
                                    stroke="white"
                                    strokeWidth="2"
                                />
                                <path
                                    d="M18 16L15 13L12 16"
                                    stroke="white"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                                <path
                                    d="M15 27V13"
                                    stroke="white"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                                <path
                                    d="M22 24L25 27L28 24"
                                    stroke="white"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                                <path
                                    d="M25 13V27"
                                    stroke="white"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                            </svg>
                            <div className="padd">
                                <div className="send-coin">
                                    <h6>مقدار و نوع ارز</h6>
                                    <div className="send-box">
                                        <input
                                            className="amount"
                                            placeholder="مقدار"
                                            type="text"
                                            onChange={(e) => {
                                                setSourcePrice(e.target.value);
                                            }}
                                        />
                                        <Select
                                            value={selectedOptionTwo}
                                            onChange={handleChangeTwo}
                                            placeholder="انتخاب"
                                            options={coins.map((i, index) => {
                                                return {
                                                    label: i,
                                                    label: (
                                                        <div>
                                                            <img
                                                                src={i.image}
                                                                alt=""
                                                            />
                                                            {i.name}
                                                        </div>
                                                    ),
                                                    value: i.id,
                                                    key: index,
                                                };
                                            })}
                                        />
                                    </div>
                                </div>
                                <div className="send-coin mt-3">
                                    <h6>مقدار و نوع ارز دریافتی </h6>
                                    <div className="send-box">
                                        <input
                                            className="amount"
                                            placeholder="مقدار دریافتی"
                                            type="text"
                                        />
                                        <Select
                                            value={selectedOption}
                                            onChange={handleChange}
                                            placeholder="انتخاب"
                                            options={coins.map((i, index) => {
                                                return {
                                                    label: i,
                                                    label: (
                                                        <div>
                                                            <img
                                                                src={i.image}
                                                                alt=""
                                                            />
                                                            {i.name}
                                                        </div>
                                                    ),
                                                    value: i.id,
                                                    key: index,
                                                };
                                            })}
                                        />
                                    </div>
                                </div>
                                <button onClick={submitChangeCoinHandler}>
                                    تبدیل
                                </button>
                            </div>
                        </div>
                    </Change>
                    <LastOrders className={stts.night == "true" ? "color-white-2 ": ""}>
                        <h5>آخرین تراکنش ها</h5>

                        <table className={stts.night == "true" ? "bg-gray last-box table-striped": "last-box table-striped"} >
                            <thead>
                                <tr>
                                    <th className="d-flex align-items-center">
                                        <div className="arrows">
                                            <svg
                                                width="6"
                                                height="5"
                                                viewBox="0 0 6 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M2.50405 1.22444C2.59165 1.11489 2.70278 1.02645 2.82919 0.965679C2.95561 0.904904 3.09407 0.873348 3.23434 0.873348C3.3746 0.873348 3.51307 0.904904 3.63949 0.965679C3.7659 1.02645 3.87702 1.11489 3.96462 1.22444L5.75901 3.46766C5.86914 3.6052 5.93816 3.77106 5.95812 3.94612C5.97809 4.12118 5.94818 4.29832 5.87186 4.45713C5.79554 4.61593 5.67589 4.74994 5.52672 4.84372C5.37755 4.93749 5.20492 4.98721 5.02873 4.98714H1.43995C1.26375 4.98721 1.09112 4.93749 0.941953 4.84372C0.792783 4.74994 0.673142 4.61593 0.596817 4.45713C0.520492 4.29832 0.49059 4.12118 0.510555 3.94612C0.53052 3.77106 0.59954 3.6052 0.709663 3.46766L2.50405 1.22444Z"
                                                    fill="#657D95"
                                                />
                                            </svg>
                                            <svg
                                                width="6"
                                                height="5"
                                                viewBox="0 0 6 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M3.96502 4.48999C3.87742 4.59954 3.76629 4.68797 3.63988 4.74875C3.51346 4.80952 3.375 4.84108 3.23473 4.84108C3.09447 4.84108 2.956 4.80952 2.82958 4.74875C2.70317 4.68797 2.59205 4.59954 2.50445 4.48999L0.710056 2.24677C0.599934 2.10923 0.530913 1.94337 0.510948 1.76831C0.490983 1.59325 0.520885 1.41611 0.59721 1.2573C0.673535 1.09849 0.793176 0.964484 0.942346 0.870713C1.09152 0.77694 1.26415 0.727223 1.44034 0.727289L5.02912 0.727289C5.20532 0.727223 5.37795 0.77694 5.52712 0.870713C5.67629 0.964484 5.79593 1.09849 5.87225 1.2573C5.94858 1.41611 5.97848 1.59325 5.95852 1.76831C5.93855 1.94337 5.86953 2.10923 5.75941 2.24677L3.96502 4.48999Z"
                                                    fill="#657D95"
                                                />
                                            </svg>
                                        </div>
                                        اسم
                                    </th>
                                    <th className="d-flex align-items-center">
                                        <div className="arrows">
                                            <svg
                                                width="6"
                                                height="5"
                                                viewBox="0 0 6 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M2.50405 1.22444C2.59165 1.11489 2.70278 1.02645 2.82919 0.965679C2.95561 0.904904 3.09407 0.873348 3.23434 0.873348C3.3746 0.873348 3.51307 0.904904 3.63949 0.965679C3.7659 1.02645 3.87702 1.11489 3.96462 1.22444L5.75901 3.46766C5.86914 3.6052 5.93816 3.77106 5.95812 3.94612C5.97809 4.12118 5.94818 4.29832 5.87186 4.45713C5.79554 4.61593 5.67589 4.74994 5.52672 4.84372C5.37755 4.93749 5.20492 4.98721 5.02873 4.98714H1.43995C1.26375 4.98721 1.09112 4.93749 0.941953 4.84372C0.792783 4.74994 0.673142 4.61593 0.596817 4.45713C0.520492 4.29832 0.49059 4.12118 0.510555 3.94612C0.53052 3.77106 0.59954 3.6052 0.709663 3.46766L2.50405 1.22444Z"
                                                    fill="#657D95"
                                                />
                                            </svg>
                                            <svg
                                                width="6"
                                                height="5"
                                                viewBox="0 0 6 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M3.96502 4.48999C3.87742 4.59954 3.76629 4.68797 3.63988 4.74875C3.51346 4.80952 3.375 4.84108 3.23473 4.84108C3.09447 4.84108 2.956 4.80952 2.82958 4.74875C2.70317 4.68797 2.59205 4.59954 2.50445 4.48999L0.710056 2.24677C0.599934 2.10923 0.530913 1.94337 0.510948 1.76831C0.490983 1.59325 0.520885 1.41611 0.59721 1.2573C0.673535 1.09849 0.793176 0.964484 0.942346 0.870713C1.09152 0.77694 1.26415 0.727223 1.44034 0.727289L5.02912 0.727289C5.20532 0.727223 5.37795 0.77694 5.52712 0.870713C5.67629 0.964484 5.79593 1.09849 5.87225 1.2573C5.94858 1.41611 5.97848 1.59325 5.95852 1.76831C5.93855 1.94337 5.86953 2.10923 5.75941 2.24677L3.96502 4.48999Z"
                                                    fill="#657D95"
                                                />
                                            </svg>
                                        </div>
                                        نوع
                                    </th>
                                    <th className="d-flex align-items-center">
                                        <div className="arrows">
                                            <svg
                                                width="6"
                                                height="5"
                                                viewBox="0 0 6 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M2.50405 1.22444C2.59165 1.11489 2.70278 1.02645 2.82919 0.965679C2.95561 0.904904 3.09407 0.873348 3.23434 0.873348C3.3746 0.873348 3.51307 0.904904 3.63949 0.965679C3.7659 1.02645 3.87702 1.11489 3.96462 1.22444L5.75901 3.46766C5.86914 3.6052 5.93816 3.77106 5.95812 3.94612C5.97809 4.12118 5.94818 4.29832 5.87186 4.45713C5.79554 4.61593 5.67589 4.74994 5.52672 4.84372C5.37755 4.93749 5.20492 4.98721 5.02873 4.98714H1.43995C1.26375 4.98721 1.09112 4.93749 0.941953 4.84372C0.792783 4.74994 0.673142 4.61593 0.596817 4.45713C0.520492 4.29832 0.49059 4.12118 0.510555 3.94612C0.53052 3.77106 0.59954 3.6052 0.709663 3.46766L2.50405 1.22444Z"
                                                    fill="#657D95"
                                                />
                                            </svg>
                                            <svg
                                                width="6"
                                                height="5"
                                                viewBox="0 0 6 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M3.96502 4.48999C3.87742 4.59954 3.76629 4.68797 3.63988 4.74875C3.51346 4.80952 3.375 4.84108 3.23473 4.84108C3.09447 4.84108 2.956 4.80952 2.82958 4.74875C2.70317 4.68797 2.59205 4.59954 2.50445 4.48999L0.710056 2.24677C0.599934 2.10923 0.530913 1.94337 0.510948 1.76831C0.490983 1.59325 0.520885 1.41611 0.59721 1.2573C0.673535 1.09849 0.793176 0.964484 0.942346 0.870713C1.09152 0.77694 1.26415 0.727223 1.44034 0.727289L5.02912 0.727289C5.20532 0.727223 5.37795 0.77694 5.52712 0.870713C5.67629 0.964484 5.79593 1.09849 5.87225 1.2573C5.94858 1.41611 5.97848 1.59325 5.95852 1.76831C5.93855 1.94337 5.86953 2.10923 5.75941 2.24677L3.96502 4.48999Z"
                                                    fill="#657D95"
                                                />
                                            </svg>
                                        </div>
                                        مقدار
                                    </th>
                                </tr>
                            </thead>
                            <div className="scrollable">
                                <tbody>
                                    {/* {
                                        orderList.map(item => {
                                            return(
                                                <tr key={}>
                                                    <td>تتر</td>
                                                    <td className="text-success-2">خرید</td>
                                                    <td>
                                                        <span>25</span>$
                                                    </td>
                                            </tr>
                                            )
                                        })
                                    } */}
                                </tbody>
                            </div>
                        </table>
                    </LastOrders>
                    {/* <YourInventory className="y-inv">
                        <h5>موجودی های شما</h5>

                        <table className="last-box table-striped">
                            <thead>
                                <tr>
                                    <th className="d-flex align-items-center">
                                        <div className="arrows">
                                            <svg
                                                width="6"
                                                height="5"
                                                viewBox="0 0 6 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M2.50405 1.22444C2.59165 1.11489 2.70278 1.02645 2.82919 0.965679C2.95561 0.904904 3.09407 0.873348 3.23434 0.873348C3.3746 0.873348 3.51307 0.904904 3.63949 0.965679C3.7659 1.02645 3.87702 1.11489 3.96462 1.22444L5.75901 3.46766C5.86914 3.6052 5.93816 3.77106 5.95812 3.94612C5.97809 4.12118 5.94818 4.29832 5.87186 4.45713C5.79554 4.61593 5.67589 4.74994 5.52672 4.84372C5.37755 4.93749 5.20492 4.98721 5.02873 4.98714H1.43995C1.26375 4.98721 1.09112 4.93749 0.941953 4.84372C0.792783 4.74994 0.673142 4.61593 0.596817 4.45713C0.520492 4.29832 0.49059 4.12118 0.510555 3.94612C0.53052 3.77106 0.59954 3.6052 0.709663 3.46766L2.50405 1.22444Z"
                                                    fill="#657D95"
                                                />
                                            </svg>
                                            <svg
                                                width="6"
                                                height="5"
                                                viewBox="0 0 6 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M3.96502 4.48999C3.87742 4.59954 3.76629 4.68797 3.63988 4.74875C3.51346 4.80952 3.375 4.84108 3.23473 4.84108C3.09447 4.84108 2.956 4.80952 2.82958 4.74875C2.70317 4.68797 2.59205 4.59954 2.50445 4.48999L0.710056 2.24677C0.599934 2.10923 0.530913 1.94337 0.510948 1.76831C0.490983 1.59325 0.520885 1.41611 0.59721 1.2573C0.673535 1.09849 0.793176 0.964484 0.942346 0.870713C1.09152 0.77694 1.26415 0.727223 1.44034 0.727289L5.02912 0.727289C5.20532 0.727223 5.37795 0.77694 5.52712 0.870713C5.67629 0.964484 5.79593 1.09849 5.87225 1.2573C5.94858 1.41611 5.97848 1.59325 5.95852 1.76831C5.93855 1.94337 5.86953 2.10923 5.75941 2.24677L3.96502 4.48999Z"
                                                    fill="#657D95"
                                                />
                                            </svg>
                                        </div>
                                        ردیف
                                    </th>
                                    <th className="d-flex align-items-center">
                                        <div className="arrows">
                                            <svg
                                                width="6"
                                                height="5"
                                                viewBox="0 0 6 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M2.50405 1.22444C2.59165 1.11489 2.70278 1.02645 2.82919 0.965679C2.95561 0.904904 3.09407 0.873348 3.23434 0.873348C3.3746 0.873348 3.51307 0.904904 3.63949 0.965679C3.7659 1.02645 3.87702 1.11489 3.96462 1.22444L5.75901 3.46766C5.86914 3.6052 5.93816 3.77106 5.95812 3.94612C5.97809 4.12118 5.94818 4.29832 5.87186 4.45713C5.79554 4.61593 5.67589 4.74994 5.52672 4.84372C5.37755 4.93749 5.20492 4.98721 5.02873 4.98714H1.43995C1.26375 4.98721 1.09112 4.93749 0.941953 4.84372C0.792783 4.74994 0.673142 4.61593 0.596817 4.45713C0.520492 4.29832 0.49059 4.12118 0.510555 3.94612C0.53052 3.77106 0.59954 3.6052 0.709663 3.46766L2.50405 1.22444Z"
                                                    fill="#657D95"
                                                />
                                            </svg>
                                            <svg
                                                width="6"
                                                height="5"
                                                viewBox="0 0 6 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M3.96502 4.48999C3.87742 4.59954 3.76629 4.68797 3.63988 4.74875C3.51346 4.80952 3.375 4.84108 3.23473 4.84108C3.09447 4.84108 2.956 4.80952 2.82958 4.74875C2.70317 4.68797 2.59205 4.59954 2.50445 4.48999L0.710056 2.24677C0.599934 2.10923 0.530913 1.94337 0.510948 1.76831C0.490983 1.59325 0.520885 1.41611 0.59721 1.2573C0.673535 1.09849 0.793176 0.964484 0.942346 0.870713C1.09152 0.77694 1.26415 0.727223 1.44034 0.727289L5.02912 0.727289C5.20532 0.727223 5.37795 0.77694 5.52712 0.870713C5.67629 0.964484 5.79593 1.09849 5.87225 1.2573C5.94858 1.41611 5.97848 1.59325 5.95852 1.76831C5.93855 1.94337 5.86953 2.10923 5.75941 2.24677L3.96502 4.48999Z"
                                                    fill="#657D95"
                                                />
                                            </svg>
                                        </div>
                                        اسم
                                    </th>
                                    <th className="d-flex align-items-center">
                                        <div className="arrows">
                                            <svg
                                                width="6"
                                                height="5"
                                                viewBox="0 0 6 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M2.50405 1.22444C2.59165 1.11489 2.70278 1.02645 2.82919 0.965679C2.95561 0.904904 3.09407 0.873348 3.23434 0.873348C3.3746 0.873348 3.51307 0.904904 3.63949 0.965679C3.7659 1.02645 3.87702 1.11489 3.96462 1.22444L5.75901 3.46766C5.86914 3.6052 5.93816 3.77106 5.95812 3.94612C5.97809 4.12118 5.94818 4.29832 5.87186 4.45713C5.79554 4.61593 5.67589 4.74994 5.52672 4.84372C5.37755 4.93749 5.20492 4.98721 5.02873 4.98714H1.43995C1.26375 4.98721 1.09112 4.93749 0.941953 4.84372C0.792783 4.74994 0.673142 4.61593 0.596817 4.45713C0.520492 4.29832 0.49059 4.12118 0.510555 3.94612C0.53052 3.77106 0.59954 3.6052 0.709663 3.46766L2.50405 1.22444Z"
                                                    fill="#657D95"
                                                />
                                            </svg>
                                            <svg
                                                width="6"
                                                height="5"
                                                viewBox="0 0 6 5"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M3.96502 4.48999C3.87742 4.59954 3.76629 4.68797 3.63988 4.74875C3.51346 4.80952 3.375 4.84108 3.23473 4.84108C3.09447 4.84108 2.956 4.80952 2.82958 4.74875C2.70317 4.68797 2.59205 4.59954 2.50445 4.48999L0.710056 2.24677C0.599934 2.10923 0.530913 1.94337 0.510948 1.76831C0.490983 1.59325 0.520885 1.41611 0.59721 1.2573C0.673535 1.09849 0.793176 0.964484 0.942346 0.870713C1.09152 0.77694 1.26415 0.727223 1.44034 0.727289L5.02912 0.727289C5.20532 0.727223 5.37795 0.77694 5.52712 0.870713C5.67629 0.964484 5.79593 1.09849 5.87225 1.2573C5.94858 1.41611 5.97848 1.59325 5.95852 1.76831C5.93855 1.94337 5.86953 2.10923 5.75941 2.24677L3.96502 4.48999Z"
                                                    fill="#657D95"
                                                />
                                            </svg>
                                        </div>
                                        مقدار
                                    </th>
                                </tr>
                            </thead>
                            <div className="scrollable w-100">
                                <tbody>
                                    <tr>
                                        <td className="d-flex align-items-center">
                                            1
                                        </td>
                                        <td className="d-flex align-items-center">
                                            <svg
                                                className="ms-1"
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M31.4972 19.8939C29.3603 28.4654 20.6787 33.6819 12.1062 31.5444C3.53727 29.4075 -1.67919 20.7254 0.458669 12.1546C2.59465 3.58216 11.2762 -1.63469 19.8461 0.502227C28.418 2.63914 33.6341 11.3222 31.497 19.8941L31.4971 19.8939H31.4972Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    d="M23.0554 13.7204C23.3738 11.5912 21.7528 10.4466 19.5361 9.68311L20.2552 6.79885L18.4994 6.36137L17.7994 9.16969C17.3378 9.05457 16.8638 8.94609 16.3927 8.83856L17.0978 6.01171L15.3432 5.57422L14.6237 8.45754C14.2417 8.37057 13.8666 8.28462 13.5026 8.19406L13.5046 8.18499L11.0834 7.58037L10.6164 9.45561C10.6164 9.45561 11.919 9.7542 11.8915 9.77258C12.6025 9.95003 12.7311 10.4207 12.7097 10.7937L11.8906 14.0796C11.9396 14.092 12.0031 14.11 12.0731 14.1381C12.0146 14.1236 11.9522 14.1077 11.8876 14.0922L10.7394 18.6952C10.6525 18.9112 10.432 19.2354 9.9349 19.1123C9.95249 19.1378 8.6588 18.7939 8.6588 18.7939L7.78711 20.8036L10.0719 21.3732C10.497 21.4798 10.9135 21.5913 11.3237 21.6962L10.5971 24.6135L12.3509 25.051L13.0704 22.1646C13.5494 22.2947 14.0144 22.4147 14.4695 22.5278L13.7525 25.4005L15.5083 25.838L16.2347 22.9261C19.2286 23.4927 21.4798 23.2643 22.4274 20.5563C23.1909 18.3761 22.3894 17.1185 20.8143 16.2984C21.9615 16.0339 22.8256 15.2793 23.056 13.7207L23.0554 13.7203L23.0554 13.7204ZM19.0439 19.3455C18.5013 21.5258 14.8305 20.3472 13.6403 20.0517L14.6045 16.1867C15.7945 16.4838 19.611 17.0718 19.044 19.3455H19.0439ZM19.5869 13.6888C19.0919 15.672 16.0366 14.6645 15.0455 14.4174L15.9197 10.9121C16.9108 11.1592 20.1025 11.6203 19.5871 13.6888H19.5869Z"
                                                    fill="white"
                                                />
                                            </svg>
                                            <span>بیت کوین</span>
                                        </td>
                                        <td className="d-flex align-items-center">
                                            BTC
                                            <span className="me-1"> 0.02 </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="d-flex align-items-center">
                                            2
                                        </td>
                                        <td className="d-flex align-items-center">
                                            <svg
                                                className="ms-1"
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M31.4972 19.8939C29.3603 28.4654 20.6787 33.6819 12.1062 31.5444C3.53727 29.4075 -1.67919 20.7254 0.458669 12.1546C2.59465 3.58216 11.2762 -1.63469 19.8461 0.502227C28.418 2.63914 33.6341 11.3222 31.497 19.8941L31.4971 19.8939H31.4972Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    d="M23.0554 13.7204C23.3738 11.5912 21.7528 10.4466 19.5361 9.68311L20.2552 6.79885L18.4994 6.36137L17.7994 9.16969C17.3378 9.05457 16.8638 8.94609 16.3927 8.83856L17.0978 6.01171L15.3432 5.57422L14.6237 8.45754C14.2417 8.37057 13.8666 8.28462 13.5026 8.19406L13.5046 8.18499L11.0834 7.58037L10.6164 9.45561C10.6164 9.45561 11.919 9.7542 11.8915 9.77258C12.6025 9.95003 12.7311 10.4207 12.7097 10.7937L11.8906 14.0796C11.9396 14.092 12.0031 14.11 12.0731 14.1381C12.0146 14.1236 11.9522 14.1077 11.8876 14.0922L10.7394 18.6952C10.6525 18.9112 10.432 19.2354 9.9349 19.1123C9.95249 19.1378 8.6588 18.7939 8.6588 18.7939L7.78711 20.8036L10.0719 21.3732C10.497 21.4798 10.9135 21.5913 11.3237 21.6962L10.5971 24.6135L12.3509 25.051L13.0704 22.1646C13.5494 22.2947 14.0144 22.4147 14.4695 22.5278L13.7525 25.4005L15.5083 25.838L16.2347 22.9261C19.2286 23.4927 21.4798 23.2643 22.4274 20.5563C23.1909 18.3761 22.3894 17.1185 20.8143 16.2984C21.9615 16.0339 22.8256 15.2793 23.056 13.7207L23.0554 13.7203L23.0554 13.7204ZM19.0439 19.3455C18.5013 21.5258 14.8305 20.3472 13.6403 20.0517L14.6045 16.1867C15.7945 16.4838 19.611 17.0718 19.044 19.3455H19.0439ZM19.5869 13.6888C19.0919 15.672 16.0366 14.6645 15.0455 14.4174L15.9197 10.9121C16.9108 11.1592 20.1025 11.6203 19.5871 13.6888H19.5869Z"
                                                    fill="white"
                                                />
                                            </svg>
                                            <span>بیت کوین</span>
                                        </td>
                                        <td className="d-flex align-items-center">
                                            BTC
                                            <span className="me-1"> 0.02 </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="d-flex align-items-center">
                                            3
                                        </td>
                                        <td className="d-flex align-items-center">
                                            <svg
                                                className="ms-1"
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M31.4972 19.8939C29.3603 28.4654 20.6787 33.6819 12.1062 31.5444C3.53727 29.4075 -1.67919 20.7254 0.458669 12.1546C2.59465 3.58216 11.2762 -1.63469 19.8461 0.502227C28.418 2.63914 33.6341 11.3222 31.497 19.8941L31.4971 19.8939H31.4972Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    d="M23.0554 13.7204C23.3738 11.5912 21.7528 10.4466 19.5361 9.68311L20.2552 6.79885L18.4994 6.36137L17.7994 9.16969C17.3378 9.05457 16.8638 8.94609 16.3927 8.83856L17.0978 6.01171L15.3432 5.57422L14.6237 8.45754C14.2417 8.37057 13.8666 8.28462 13.5026 8.19406L13.5046 8.18499L11.0834 7.58037L10.6164 9.45561C10.6164 9.45561 11.919 9.7542 11.8915 9.77258C12.6025 9.95003 12.7311 10.4207 12.7097 10.7937L11.8906 14.0796C11.9396 14.092 12.0031 14.11 12.0731 14.1381C12.0146 14.1236 11.9522 14.1077 11.8876 14.0922L10.7394 18.6952C10.6525 18.9112 10.432 19.2354 9.9349 19.1123C9.95249 19.1378 8.6588 18.7939 8.6588 18.7939L7.78711 20.8036L10.0719 21.3732C10.497 21.4798 10.9135 21.5913 11.3237 21.6962L10.5971 24.6135L12.3509 25.051L13.0704 22.1646C13.5494 22.2947 14.0144 22.4147 14.4695 22.5278L13.7525 25.4005L15.5083 25.838L16.2347 22.9261C19.2286 23.4927 21.4798 23.2643 22.4274 20.5563C23.1909 18.3761 22.3894 17.1185 20.8143 16.2984C21.9615 16.0339 22.8256 15.2793 23.056 13.7207L23.0554 13.7203L23.0554 13.7204ZM19.0439 19.3455C18.5013 21.5258 14.8305 20.3472 13.6403 20.0517L14.6045 16.1867C15.7945 16.4838 19.611 17.0718 19.044 19.3455H19.0439ZM19.5869 13.6888C19.0919 15.672 16.0366 14.6645 15.0455 14.4174L15.9197 10.9121C16.9108 11.1592 20.1025 11.6203 19.5871 13.6888H19.5869Z"
                                                    fill="white"
                                                />
                                            </svg>
                                            <span>بیت کوین</span>
                                        </td>
                                        <td className="d-flex align-items-center">
                                            BTC
                                            <span className="me-1"> 0.02 </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="d-flex align-items-center">
                                            1
                                        </td>
                                        <td className="d-flex align-items-center">
                                            <svg
                                                className="ms-1"
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M31.4972 19.8939C29.3603 28.4654 20.6787 33.6819 12.1062 31.5444C3.53727 29.4075 -1.67919 20.7254 0.458669 12.1546C2.59465 3.58216 11.2762 -1.63469 19.8461 0.502227C28.418 2.63914 33.6341 11.3222 31.497 19.8941L31.4971 19.8939H31.4972Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    d="M23.0554 13.7204C23.3738 11.5912 21.7528 10.4466 19.5361 9.68311L20.2552 6.79885L18.4994 6.36137L17.7994 9.16969C17.3378 9.05457 16.8638 8.94609 16.3927 8.83856L17.0978 6.01171L15.3432 5.57422L14.6237 8.45754C14.2417 8.37057 13.8666 8.28462 13.5026 8.19406L13.5046 8.18499L11.0834 7.58037L10.6164 9.45561C10.6164 9.45561 11.919 9.7542 11.8915 9.77258C12.6025 9.95003 12.7311 10.4207 12.7097 10.7937L11.8906 14.0796C11.9396 14.092 12.0031 14.11 12.0731 14.1381C12.0146 14.1236 11.9522 14.1077 11.8876 14.0922L10.7394 18.6952C10.6525 18.9112 10.432 19.2354 9.9349 19.1123C9.95249 19.1378 8.6588 18.7939 8.6588 18.7939L7.78711 20.8036L10.0719 21.3732C10.497 21.4798 10.9135 21.5913 11.3237 21.6962L10.5971 24.6135L12.3509 25.051L13.0704 22.1646C13.5494 22.2947 14.0144 22.4147 14.4695 22.5278L13.7525 25.4005L15.5083 25.838L16.2347 22.9261C19.2286 23.4927 21.4798 23.2643 22.4274 20.5563C23.1909 18.3761 22.3894 17.1185 20.8143 16.2984C21.9615 16.0339 22.8256 15.2793 23.056 13.7207L23.0554 13.7203L23.0554 13.7204ZM19.0439 19.3455C18.5013 21.5258 14.8305 20.3472 13.6403 20.0517L14.6045 16.1867C15.7945 16.4838 19.611 17.0718 19.044 19.3455H19.0439ZM19.5869 13.6888C19.0919 15.672 16.0366 14.6645 15.0455 14.4174L15.9197 10.9121C16.9108 11.1592 20.1025 11.6203 19.5871 13.6888H19.5869Z"
                                                    fill="white"
                                                />
                                            </svg>
                                            <span>بیت کوین</span>
                                        </td>
                                        <td className="d-flex align-items-center">
                                            BTC
                                            <span className="me-1"> 0.02 </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="d-flex align-items-center">
                                            2
                                        </td>
                                        <td className="d-flex align-items-center">
                                            <svg
                                                className="ms-1"
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M31.4972 19.8939C29.3603 28.4654 20.6787 33.6819 12.1062 31.5444C3.53727 29.4075 -1.67919 20.7254 0.458669 12.1546C2.59465 3.58216 11.2762 -1.63469 19.8461 0.502227C28.418 2.63914 33.6341 11.3222 31.497 19.8941L31.4971 19.8939H31.4972Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    d="M23.0554 13.7204C23.3738 11.5912 21.7528 10.4466 19.5361 9.68311L20.2552 6.79885L18.4994 6.36137L17.7994 9.16969C17.3378 9.05457 16.8638 8.94609 16.3927 8.83856L17.0978 6.01171L15.3432 5.57422L14.6237 8.45754C14.2417 8.37057 13.8666 8.28462 13.5026 8.19406L13.5046 8.18499L11.0834 7.58037L10.6164 9.45561C10.6164 9.45561 11.919 9.7542 11.8915 9.77258C12.6025 9.95003 12.7311 10.4207 12.7097 10.7937L11.8906 14.0796C11.9396 14.092 12.0031 14.11 12.0731 14.1381C12.0146 14.1236 11.9522 14.1077 11.8876 14.0922L10.7394 18.6952C10.6525 18.9112 10.432 19.2354 9.9349 19.1123C9.95249 19.1378 8.6588 18.7939 8.6588 18.7939L7.78711 20.8036L10.0719 21.3732C10.497 21.4798 10.9135 21.5913 11.3237 21.6962L10.5971 24.6135L12.3509 25.051L13.0704 22.1646C13.5494 22.2947 14.0144 22.4147 14.4695 22.5278L13.7525 25.4005L15.5083 25.838L16.2347 22.9261C19.2286 23.4927 21.4798 23.2643 22.4274 20.5563C23.1909 18.3761 22.3894 17.1185 20.8143 16.2984C21.9615 16.0339 22.8256 15.2793 23.056 13.7207L23.0554 13.7203L23.0554 13.7204ZM19.0439 19.3455C18.5013 21.5258 14.8305 20.3472 13.6403 20.0517L14.6045 16.1867C15.7945 16.4838 19.611 17.0718 19.044 19.3455H19.0439ZM19.5869 13.6888C19.0919 15.672 16.0366 14.6645 15.0455 14.4174L15.9197 10.9121C16.9108 11.1592 20.1025 11.6203 19.5871 13.6888H19.5869Z"
                                                    fill="white"
                                                />
                                            </svg>
                                            <span>بیت کوین</span>
                                        </td>
                                        <td className="d-flex align-items-center">
                                            BTC
                                            <span className="me-1"> 0.02 </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="d-flex align-items-center">
                                            3
                                        </td>
                                        <td className="d-flex align-items-center">
                                            <svg
                                                className="ms-1"
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M31.4972 19.8939C29.3603 28.4654 20.6787 33.6819 12.1062 31.5444C3.53727 29.4075 -1.67919 20.7254 0.458669 12.1546C2.59465 3.58216 11.2762 -1.63469 19.8461 0.502227C28.418 2.63914 33.6341 11.3222 31.497 19.8941L31.4971 19.8939H31.4972Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    d="M23.0554 13.7204C23.3738 11.5912 21.7528 10.4466 19.5361 9.68311L20.2552 6.79885L18.4994 6.36137L17.7994 9.16969C17.3378 9.05457 16.8638 8.94609 16.3927 8.83856L17.0978 6.01171L15.3432 5.57422L14.6237 8.45754C14.2417 8.37057 13.8666 8.28462 13.5026 8.19406L13.5046 8.18499L11.0834 7.58037L10.6164 9.45561C10.6164 9.45561 11.919 9.7542 11.8915 9.77258C12.6025 9.95003 12.7311 10.4207 12.7097 10.7937L11.8906 14.0796C11.9396 14.092 12.0031 14.11 12.0731 14.1381C12.0146 14.1236 11.9522 14.1077 11.8876 14.0922L10.7394 18.6952C10.6525 18.9112 10.432 19.2354 9.9349 19.1123C9.95249 19.1378 8.6588 18.7939 8.6588 18.7939L7.78711 20.8036L10.0719 21.3732C10.497 21.4798 10.9135 21.5913 11.3237 21.6962L10.5971 24.6135L12.3509 25.051L13.0704 22.1646C13.5494 22.2947 14.0144 22.4147 14.4695 22.5278L13.7525 25.4005L15.5083 25.838L16.2347 22.9261C19.2286 23.4927 21.4798 23.2643 22.4274 20.5563C23.1909 18.3761 22.3894 17.1185 20.8143 16.2984C21.9615 16.0339 22.8256 15.2793 23.056 13.7207L23.0554 13.7203L23.0554 13.7204ZM19.0439 19.3455C18.5013 21.5258 14.8305 20.3472 13.6403 20.0517L14.6045 16.1867C15.7945 16.4838 19.611 17.0718 19.044 19.3455H19.0439ZM19.5869 13.6888C19.0919 15.672 16.0366 14.6645 15.0455 14.4174L15.9197 10.9121C16.9108 11.1592 20.1025 11.6203 19.5871 13.6888H19.5869Z"
                                                    fill="white"
                                                />
                                            </svg>
                                            <span>بیت کوین</span>
                                        </td>
                                        <td className="d-flex align-items-center">
                                            BTC
                                            <span className="me-1"> 0.02 </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="d-flex align-items-center">
                                            1
                                        </td>
                                        <td className="d-flex align-items-center">
                                            <svg
                                                className="ms-1"
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M31.4972 19.8939C29.3603 28.4654 20.6787 33.6819 12.1062 31.5444C3.53727 29.4075 -1.67919 20.7254 0.458669 12.1546C2.59465 3.58216 11.2762 -1.63469 19.8461 0.502227C28.418 2.63914 33.6341 11.3222 31.497 19.8941L31.4971 19.8939H31.4972Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    d="M23.0554 13.7204C23.3738 11.5912 21.7528 10.4466 19.5361 9.68311L20.2552 6.79885L18.4994 6.36137L17.7994 9.16969C17.3378 9.05457 16.8638 8.94609 16.3927 8.83856L17.0978 6.01171L15.3432 5.57422L14.6237 8.45754C14.2417 8.37057 13.8666 8.28462 13.5026 8.19406L13.5046 8.18499L11.0834 7.58037L10.6164 9.45561C10.6164 9.45561 11.919 9.7542 11.8915 9.77258C12.6025 9.95003 12.7311 10.4207 12.7097 10.7937L11.8906 14.0796C11.9396 14.092 12.0031 14.11 12.0731 14.1381C12.0146 14.1236 11.9522 14.1077 11.8876 14.0922L10.7394 18.6952C10.6525 18.9112 10.432 19.2354 9.9349 19.1123C9.95249 19.1378 8.6588 18.7939 8.6588 18.7939L7.78711 20.8036L10.0719 21.3732C10.497 21.4798 10.9135 21.5913 11.3237 21.6962L10.5971 24.6135L12.3509 25.051L13.0704 22.1646C13.5494 22.2947 14.0144 22.4147 14.4695 22.5278L13.7525 25.4005L15.5083 25.838L16.2347 22.9261C19.2286 23.4927 21.4798 23.2643 22.4274 20.5563C23.1909 18.3761 22.3894 17.1185 20.8143 16.2984C21.9615 16.0339 22.8256 15.2793 23.056 13.7207L23.0554 13.7203L23.0554 13.7204ZM19.0439 19.3455C18.5013 21.5258 14.8305 20.3472 13.6403 20.0517L14.6045 16.1867C15.7945 16.4838 19.611 17.0718 19.044 19.3455H19.0439ZM19.5869 13.6888C19.0919 15.672 16.0366 14.6645 15.0455 14.4174L15.9197 10.9121C16.9108 11.1592 20.1025 11.6203 19.5871 13.6888H19.5869Z"
                                                    fill="white"
                                                />
                                            </svg>
                                            <span>بیت کوین</span>
                                        </td>
                                        <td className="d-flex align-items-center">
                                            BTC
                                            <span className="me-1"> 0.02 </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="d-flex align-items-center">
                                            2
                                        </td>
                                        <td className="d-flex align-items-center">
                                            <svg
                                                className="ms-1"
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M31.4972 19.8939C29.3603 28.4654 20.6787 33.6819 12.1062 31.5444C3.53727 29.4075 -1.67919 20.7254 0.458669 12.1546C2.59465 3.58216 11.2762 -1.63469 19.8461 0.502227C28.418 2.63914 33.6341 11.3222 31.497 19.8941L31.4971 19.8939H31.4972Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    d="M23.0554 13.7204C23.3738 11.5912 21.7528 10.4466 19.5361 9.68311L20.2552 6.79885L18.4994 6.36137L17.7994 9.16969C17.3378 9.05457 16.8638 8.94609 16.3927 8.83856L17.0978 6.01171L15.3432 5.57422L14.6237 8.45754C14.2417 8.37057 13.8666 8.28462 13.5026 8.19406L13.5046 8.18499L11.0834 7.58037L10.6164 9.45561C10.6164 9.45561 11.919 9.7542 11.8915 9.77258C12.6025 9.95003 12.7311 10.4207 12.7097 10.7937L11.8906 14.0796C11.9396 14.092 12.0031 14.11 12.0731 14.1381C12.0146 14.1236 11.9522 14.1077 11.8876 14.0922L10.7394 18.6952C10.6525 18.9112 10.432 19.2354 9.9349 19.1123C9.95249 19.1378 8.6588 18.7939 8.6588 18.7939L7.78711 20.8036L10.0719 21.3732C10.497 21.4798 10.9135 21.5913 11.3237 21.6962L10.5971 24.6135L12.3509 25.051L13.0704 22.1646C13.5494 22.2947 14.0144 22.4147 14.4695 22.5278L13.7525 25.4005L15.5083 25.838L16.2347 22.9261C19.2286 23.4927 21.4798 23.2643 22.4274 20.5563C23.1909 18.3761 22.3894 17.1185 20.8143 16.2984C21.9615 16.0339 22.8256 15.2793 23.056 13.7207L23.0554 13.7203L23.0554 13.7204ZM19.0439 19.3455C18.5013 21.5258 14.8305 20.3472 13.6403 20.0517L14.6045 16.1867C15.7945 16.4838 19.611 17.0718 19.044 19.3455H19.0439ZM19.5869 13.6888C19.0919 15.672 16.0366 14.6645 15.0455 14.4174L15.9197 10.9121C16.9108 11.1592 20.1025 11.6203 19.5871 13.6888H19.5869Z"
                                                    fill="white"
                                                />
                                            </svg>
                                            <span>بیت کوین</span>
                                        </td>
                                        <td className="d-flex align-items-center">
                                            BTC
                                            <span className="me-1"> 0.02 </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="d-flex align-items-center">
                                            3
                                        </td>
                                        <td className="d-flex align-items-center">
                                            <svg
                                                className="ms-1"
                                                width="32"
                                                height="32"
                                                viewBox="0 0 32 32"
                                                fill="none"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path
                                                    d="M31.4972 19.8939C29.3603 28.4654 20.6787 33.6819 12.1062 31.5444C3.53727 29.4075 -1.67919 20.7254 0.458669 12.1546C2.59465 3.58216 11.2762 -1.63469 19.8461 0.502227C28.418 2.63914 33.6341 11.3222 31.497 19.8941L31.4971 19.8939H31.4972Z"
                                                    fill="#F7931A"
                                                />
                                                <path
                                                    d="M23.0554 13.7204C23.3738 11.5912 21.7528 10.4466 19.5361 9.68311L20.2552 6.79885L18.4994 6.36137L17.7994 9.16969C17.3378 9.05457 16.8638 8.94609 16.3927 8.83856L17.0978 6.01171L15.3432 5.57422L14.6237 8.45754C14.2417 8.37057 13.8666 8.28462 13.5026 8.19406L13.5046 8.18499L11.0834 7.58037L10.6164 9.45561C10.6164 9.45561 11.919 9.7542 11.8915 9.77258C12.6025 9.95003 12.7311 10.4207 12.7097 10.7937L11.8906 14.0796C11.9396 14.092 12.0031 14.11 12.0731 14.1381C12.0146 14.1236 11.9522 14.1077 11.8876 14.0922L10.7394 18.6952C10.6525 18.9112 10.432 19.2354 9.9349 19.1123C9.95249 19.1378 8.6588 18.7939 8.6588 18.7939L7.78711 20.8036L10.0719 21.3732C10.497 21.4798 10.9135 21.5913 11.3237 21.6962L10.5971 24.6135L12.3509 25.051L13.0704 22.1646C13.5494 22.2947 14.0144 22.4147 14.4695 22.5278L13.7525 25.4005L15.5083 25.838L16.2347 22.9261C19.2286 23.4927 21.4798 23.2643 22.4274 20.5563C23.1909 18.3761 22.3894 17.1185 20.8143 16.2984C21.9615 16.0339 22.8256 15.2793 23.056 13.7207L23.0554 13.7203L23.0554 13.7204ZM19.0439 19.3455C18.5013 21.5258 14.8305 20.3472 13.6403 20.0517L14.6045 16.1867C15.7945 16.4838 19.611 17.0718 19.044 19.3455H19.0439ZM19.5869 13.6888C19.0919 15.672 16.0366 14.6645 15.0455 14.4174L15.9197 10.9121C16.9108 11.1592 20.1025 11.6203 19.5871 13.6888H19.5869Z"
                                                    fill="white"
                                                />
                                            </svg>
                                            <span>بیت کوین</span>
                                        </td>
                                        <td className="d-flex align-items-center">
                                            BTC
                                            <span className="me-1"> 0.02 </span>
                                        </td>
                                    </tr>
                                </tbody>
                            </div>
                        </table>
                    </YourInventory> */}
                </div>
            </Content>
        </Main>
    );
}
