import React, { useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import styled from "styled-components";
import QRCode from "react-qr-code";
import { baseUrl } from "../BaseUrl";
import axios from "axios";

const Main = styled.div`
    z-index: 10;
    .box {
        width: 595px;
        height: 285px;
        background: #ffffff;
        border-radius: 16px;
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        padding: 32px;
        span {
            font-weight: 600;
            font-size: 16px;
        }
        p {
            font-weight: 500;
            font-size: 14px;
            line-height: 20px;
            text-align: right;
            max-width: 320px;
            color: #29335c;
        }
        h6 {
            font-size: 16px;
            font-weight: 500;
            color: #4c4c4c;
            margin-top: 28px;
            margin-bottom: 6px;
        }
        .adress-box {
            width: 320px;
            height: 44px;
            background: #ffffff;
            border: 1.5px solid #dbdbdb;
            border-radius: 8px;
            padding: 10px 16px;
            display: flex;
            align-items: center;
            justify-content: space-between;
            color: #6d6d6d;
            font-weight: normal;
            font-size: 13.5px;
            white-space: nowrap;
        }
        .qr-code {
            transform: scale(0.5);
            transform-origin: top;
        }
        .text-red {
            font-weight: 500;
            font-size: 26px;
            line-height: 70px;
            margin-right: 20px;
            color: #f15152;
        }
    }
    @media (max-width: 992px) {
        .box {
            width: 90% !important;
            z-index: 1;
        }
    }
    @media (max-width: 786px) {
        .to-col {
            flex-direction: column;
            align-items: center;
        }
        .box {
            height: 520px !important;
        }
        .qr-code {
            margin-top: 40px;
        }
        .adress-box {
            width: 280px !important;
            margin-right: 0;
            font-size: 12px;
        }
    }
    
`;

const CoinDeposit = (props) => {
    const wallet = props.wallet;
    const itemTo = props.itemTo;
    const [adress, setAdress] = useState("");
    let token = "";
    setTimeout(() => {
        token = localStorage.getItem("token");
    }, 1000);
    let item = wallet.find((i) => {
        if (i.service !== undefined) {
            return i.service.small_name_slug == itemTo.small_name_slug;
        }
    });
    console.log(props.token);
    useEffect(() => {
        setTimeout(() => {
            let data = {
                wallet: item.id,
            };
            let config = {
                method: "POST",
                url: `${baseUrl}wallet/deposit/address/`,
                data: data,
                headers: {
                    "Content-type": "application/json",
                    Authorization: `Bearer ${token}`,
                },
            };

            axios(config)
                .then((response) => {
                    // setAdress(response.data)
                })
                .catch((error) => {});
        }, 2000);
    }, []);
    console.log(item);
    return (
        <Main>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <div className={props.stts.night == "true" ? "bg-gray box" : " box"}>
                <div className="d-flex justify-content-between align-items-center mb-4">
                    <span>واریز به کیف پول شما</span>
                    <svg
                        onClick={() => {
                            props.setBlur(false);
                            props.setShowCoinDeposit(false);
                        }}
                        className="c-p"
                        width="32"
                        height="32"
                        viewBox="0 0 32 32"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M12 20L20 12"
                            stroke="#777777"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        />
                        <path
                            d="M20 20L12 12"
                            stroke="#777777"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        />
                    </svg>
                </div>
                <div className="d-flex to-col justify-content-between">
                    <div>
                        <p>
                            آدرس کیف پول شما در کادر زیر قابل مشاهده است برای
                            واریز کردن ارزهای دیجیتال به این کیف پول میتوانید از
                            این آدرس استفاده کنید.
                        </p>
                        <h6>آدرس کیف پول شما</h6>
                        <div className="adress-box">
                            <div>{adress}</div>

                            <svg
                                onClick={() => {
                                    navigator.clipboard.writeText(adress);
                                    toast.success("آدرس کپی شد", {
                                        position: "top-center",
                                        autoClose: 5000,
                                        hideProgressBar: false,
                                        closeOnClick: true,
                                        pauseOnHover: true,
                                        draggable: true,
                                        progress: undefined,
                                    });
                                }}
                                className="c-p"
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <rect
                                    x="6.99792"
                                    y="6.99792"
                                    width="14.0058"
                                    height="14.0058"
                                    rx="2"
                                    stroke="#727272"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                                <path
                                    d="M6.99792 17.0021H4.99709C3.89206 17.0021 2.99625 16.1063 2.99625 15.0013V4.99709C2.99625 3.89206 3.89206 2.99625 4.99709 2.99625H15.0013C16.1063 2.99625 17.0021 3.89206 17.0021 4.99709V6.99792"
                                    stroke="#727272"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                            </svg>
                        </div>
                    </div>
                    <div className="qr-code">
                        <QRCode value={adress} />
                        <div className="text-red">بارکد آدرس کیف پول</div>
                    </div>
                </div>
            </div>
        </Main>
    );
};

export default CoinDeposit;
