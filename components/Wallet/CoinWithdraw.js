import React, { useEffect, useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import styled from "styled-components";
import QRCode from "react-qr-code";
import { baseUrl } from "../BaseUrl";
import axios from "axios";

const Main = styled.div`
    z-index: 10;
    .box {
        width: 595px;
        height: 431px;
        background: #ffffff;
        border-radius: 16px;
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        padding: 32px;
        p {
            font-weight: 500;
            font-size: 14px;
            line-height: 20px;
            text-align: right;
            color: #29335c;
        }
        .text-red {
            color: #f15152;
            font-weight: 500;
            font-size: 14px;
            line-height: 20px;
        }
    }
    @media (max-width: 992px) {
        .box {
            width: 90% !important;
            z-index: 1;
        }
    }
    @media (max-width: 786px) {
        .to-col {
            flex-direction: column;
            align-items: center;
        }
        .box {
            height: 520px !important;
        }
    }
`;

const InputBox = styled.div`
    width: 100%;
    position: relative;
    input {
        background: #ffffff;
        border: 1.5px solid #dbdbdb;
        box-sizing: border-box;
        border-radius: 8px;
        padding: 10px;
        width: 100%;
        margin-top: 7px;
    }
    button {
        position: absolute;
        width: 86px;
        height: 31px;
        background: rgba(16, 138, 187, 0.3);
        border-radius: 61px;
        left: 16px;
        top: 14px;
        font-size: 12px;
        font-weight: 500;
        color: #29335c;
    }
`;

const Submit = styled.button`
    width: 193px;
    height: 37px;
    background: linear-gradient(90deg, #128cbd -1.72%, #3dbdc8 100%);
    border-radius: 32px;
    color: #ffff;
    font-weight: 500;
    font-size: 14px;
    margin-right: auto;
    margin-left: auto;
    margin-top: 24px;
    transition:.5s;
    :hover {
        opacity:.9;
    }
`;

const CoinWithdraw = (props) => {
    const wallet = props.wallet;
    const itemTo = props.itemTo;
    const [adress, setAdress] = useState("");
    let token = "";
    setTimeout(() => {
        token = localStorage.getItem("token");
    }, 1000);
    let item = wallet.find((i) => {
        if (i.service !== undefined) {
            return i.service.small_name_slug == itemTo.small_name_slug;
        }
    });
    console.log(item);
    useEffect(() => {
        setTimeout(() => {
            let data = {
                wallet: item.id,
            };
            let config = {
                method: "POST",
                url: `${baseUrl}`,
                data: data,
                headers: {
                    "Content-type": "application/json",
                    Authorization: `Bearer ${token}`,
                },
            };

            axios(config)
                .then((response) => {
                    // setAdress(response.data)
                })
                .catch((error) => {});
        }, 2000);
    }, []);
    return (
        <Main>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <div  className={props.stts.night == "true" ? "bg-gray box" : " box"}>
                <div className="d-flex justify-content-between align-items-center mb-4">
                    <span>برداشت از کیف پول شما</span>
                    <svg
                        onClick={() => {
                            props.setBlur(false);
                            props.setShowCoinWithDrow(false);
                        }}
                        className="c-p"
                        width="32"
                        height="32"
                        viewBox="0 0 32 32"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            d="M12 20L20 12"
                            stroke="#777777"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        />
                        <path
                            d="M20 20L12 12"
                            stroke="#777777"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        />
                    </svg>
                </div>
                <p>
                    در صورت تمایل به برداشت موجودی کیف پول های خود ، درخواست خود
                    را اینجا ثبت نمایید.
                </p>
                <p className="text-red">
                    کارمزد انتقال در شبکه {item.service.name} :{" "}
                    {item.service.trade_fee} {item.service.small_name_slug}
                </p>
                <span>میزان برداشت {item.service.name} </span>
                <InputBox>
                    <input type="text" />
                    <button  className={props.stts.night == "true" ? "color-white-2" : " "}>کل موجودی</button>
                </InputBox>
                <span className="d-block mt-4">آدرس کیف پول مقصد</span>
                <InputBox>
                    <input type="text" />
                </InputBox>
                <div className="w-100 d-flex justify-content-center">
                <Submit>ثبت درخواست برداشت</Submit>
                </div>
            </div>
        </Main>
    );
};

export default CoinWithdraw;
