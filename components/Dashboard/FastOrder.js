import axios from "axios";
import React, { useState } from "react";
import Select from "react-select";
import styled from "styled-components";

const Main = styled.div`
    padding: 32px;
    padding-right: 0;
    padding-left: 0;
    h6 {
        font-weight: 600;
        font-size: 18px;
    }
    @media (max-width: 786px) {
        padding: 0;
        margin-top: 32px;
    }
`;

const Box = styled.div`
    background: #ffffff;
    box-shadow: 0px 2px 8px rgba(50, 50, 50, 0.12);
    border-radius: 16px;
    width: 390px;
    height: 280px;
    margin-top: 16px;
    padding: 20px;

    .buy-btn {
        margin-top: 34px;
        color: #fff;
        width: 358px;
        height: 40px;
        border-radius: 8px;
        background: linear-gradient(90deg, #128cbd -1.72%, #3dbdc8 100%);
    }
    @media (max-width: 786px) {
        width: 343px;
        height: 350px;
        .d-flex {
            flex-wrap: wrap;
        }
        .buy-btn {
            margin-top: 16px;
            width: 305px;
        }
    }
    .sell-btn {
        background: linear-gradient(
            90deg,
            #ff3c00 -1.72%,
            #ff792b 100%
        ) !important;
    }
`;

const BoxHead = styled.div`
    width: 163px;
    height: 32px;
    background-color: #edf8fc;
    border-radius: 6px;
    padding: 4px;
    button {
        width: 75px;
    }
    .buy-active {
        background: #108abb;
        color: #fff;
        border-radius: 10px 4px 4px 10px;
    }
    .sell-active {
        background: #108abb;
        color: #fff;
        border-radius: 4px 10px 10px 4px;
    }
`;

const Inventory = styled.div`
    border-bottom: 1px solid #b3b3b3;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 16px 0;
`;

const SelectCoin = styled.div`
    h5 {
        font-size: 16px;
        font-weight: 400;
        margin-top: 16px;
        color: #323232;
    }
`;
const Amount = styled.div`
    h5 {
        font-size: 16px;
        font-weight: 400;
        margin-top: 16px;
        color: #323232;
    }
    input {
        width: 168px;
        height: 42px;
        background: #edf8fc;
        border: 1px solid #dedede;
        border-radius: 8px;
        padding: 0 8px;
        text-align: left;
        ::-webkit-outer-spin-button,
        ::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        -moz-appearance: textfield;
    }
    @media (max-width: 786px) {
        input {
            width: 305px;
        }
    }
`;

const FastOrder = (props) => {
    const [activeTab, setActiveTab] = useState("buy");
    const [selectedCoin, setselectedCoin] = useState(null);
    const [selectedCoinTwo, setselectedCoinTwo] = useState(null);
    let tokens = "";
    setTimeout(() => {
        tokens = localStorage.getItem("token");
    }, 1000);


    const handleChange = (selectedCoin) => {
        setselectedCoin(selectedCoin);
    };
    const handleChangeTwo = (selectedCoinTwo) => {
        setselectedCoinTwo(selectedCoinTwo);
    };

    const sellHandler = (e) => {
        e.preventDefault();
        let data = {
            source: selectedOptionTwo.value,
            destination: selectedOption.value,
            changed: "source" ,
            "source-price": sourcePrice ,
            "destination-price": 0
        };
        let config = {
            headers: {
                "Content-type": "application/json",
                Authorization: `Bearer ${tokens}`,
            },
            method: "POST",
            url: `${baseUrl}order/calculator/`,
            data: data,
        };

         axios(config)
            .then((response) => {
            })
            .catch((error) => {
                toast.error("خطایی وجود دارد", {
                    position: "top-center",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            });
    };
    const buyHandler = (e) => {};
    return (
        <Main>
            <h6 className={props.night == "true" ? "color-white-2": ""}>سفارش سریع</h6>
            <Box className={props.night == "true" ? "bg-gray ": ""}>
                <BoxHead className={props.night == "true" ? "bg-dark-2": ""}>
                    <button
                        onClick={() => {
                            setActiveTab("buy");
                        }}
                        className={activeTab === "buy" ? "buy-active" : props.night == "true" ? "color-white-2": ""}
                        type="button"
                    >
                        خرید
                    </button>
                    <button
                        onClick={() => {
                            setActiveTab("sell");
                        }}
                        className={activeTab === "sell" ? "sell-active" : props.night == "true" ? "color-white-2": ""}
                        type="button"
                    >
                        فروش
                    </button>
                </BoxHead>
                <Inventory className={props.night == "true" ? "color-white-2": ""}>
                    <span>موجودی شما :</span>
                    <span>
                        <span>121,200</span>ریال
                    </span>
                </Inventory>
                {activeTab === "buy" ? (
                    <>
                        <div className="d-flex justify-content-between">
                            <SelectCoin>
                                <h5 className={props.night == "true" ? "color-white-2": ""}>انتخاب ارز</h5>
                                <Select
                                 
                                    value={selectedCoinTwo}
                                    onChange={handleChangeTwo}
                                    placeholder="انتخاب"
                                    options={props.coins.map((i, index) => {
                                        return {
                                            label: i,
                                            label: (
                                                <div>
                                                    <img src={i.image} alt="" />
                                                    {i.name}
                                                </div>
                                            ),
                                            value: i.small_name_slug,
                                            key: index,
                                        };
                                    })}
                                />
                            </SelectCoin>
                            <Amount>
                                <h5 className={props.night == "true" ? "color-white-2": ""}>مقدار</h5>
                                <input
                                 className={props.night == "true" ? "bg-dark-2": ""} type="number" placeholder="تومان" />
                            </Amount>
                        </div>
                        <button className="buy-btn" onClick={buyHandler}>
                            خرید
                        </button>
                    </>
                ) : (
                    <>
                        <div className="d-flex justify-content-between">
                            <SelectCoin>
                                <h5>انتخاب ارز</h5>
                                <Select
                                    value={selectedCoin}
                                    onChange={handleChange}
                                    placeholder="انتخاب"
                                    options={props.coins.map((i, index) => {
                                        return {
                                            label: i,
                                            label: (
                                                <div>
                                                    <img src={i.image} alt="" />
                                                    {i.name}
                                                </div>
                                            ),
                                            value: i.small_name_slug,
                                            key: index,
                                        };
                                    })}
                                />
                            </SelectCoin>
                            <Amount>
                                <h5>مقدار</h5>
                                <input className={props.night == "true" ? "bg-dark-2": ""} type="number" placeholder="تومان" />
                            </Amount>
                        </div>
                        <button
                            className="buy-btn sell-btn"
                            onClick={sellHandler}
                        >
                            فروش
                        </button>
                    </>
                )}
            </Box>
        </Main>
    );
};

export default FastOrder;
